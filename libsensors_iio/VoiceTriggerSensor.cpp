#include <fcntl.h>
#include <errno.h>
#include <unistd.h>
#include <sys/select.h>
#include <sys/stat.h>
#include <cutils/log.h>
#include <hardware/sensors.h>

#include "VoiceTriggerSensor.h"

static sensor_t sSensor = {
        SENSOR_NAME,
        "Ingenic",
        1,
        SENSORS_VOICE_TRIGGER_HANDLE,
        SENSOR_TYPE_VOICE_TRIGGER,
        1100.0f,
        0.005f,
        0.005f,
        10000,
        0, 0,
        SENSOR_STRING_TYPE_VOICE_TRIGGER,
        "", 10000,
        SENSOR_FLAG_WAKE_UP,
        { }
};

VoiceTriggerSensor::VoiceTriggerSensor()
        : SensorBase(NULL, NULL),
          mExist(false),
          mEnabled(false),
          mMainReadFd(-1),
          mWorkerWriteFd(-1),
          mStartThread(false),
          mExit(false)
{
    mExist = scanSensor();
    if (mExist) {
        if (!initialize()) {
            mExist = false;
            LOGI("Voice trigger sensor initialize faliure.");
        } else
            LOGI("Voice trigger sensor initialize done.");
    } else {
        LOGI("No Voice trigger sensor!");
    }
}

VoiceTriggerSensor::~VoiceTriggerSensor()
{
    if (mExist) {

        pthread_mutex_lock(&mStartLock);
        mStartThread = false;
        mExit = true;
        pthread_mutex_unlock(&mStartLock);

        close(mMainReadFd);
        close(mWorkerWriteFd);

        if (mEnabled)
            enable(0);
    }
}

bool VoiceTriggerSensor::scanSensor() const
{
    int error = -1;

    error = access(DEVICE_CONFIG_PATH, W_OK);
    if (error < 0) {
        LOGE("%s access error: %s", DEVICE_CONFIG_PATH, strerror(error));
        return false;
    }

    error = access(RESOURCE_PATH, R_OK);
    if (error < 0) {
        LOGE("%s access error: %s", RESOURCE_PATH, strerror(error));
        return false;
    }

    error = access(SYSFS_PATH, R_OK | W_OK);
    if (error < 0) {
        LOGE("%s access error: %s", SYSFS_PATH, strerror(error));
        return false;
    }

    return true;
}

bool VoiceTriggerSensor::initialize()
{
    int retval = 0;

    int pipeFd[2];
    retval = pipe(pipeFd);
    if (retval < 0) {
        LOGE("Failed to create pipe: %s", strerror(errno));
        return false;
    }
    mMainReadFd = pipeFd[0];
    mWorkerWriteFd = pipeFd[1];

    pthread_t tid;
    pthread_attr_t attr;
    pthread_attr_init(&attr);
    pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_DETACHED);

    retval = pthread_create(&tid, &attr, threadLoop, this);
    if (retval) {
        LOGE("Failed to create poll thread %s", strerror(retval));
        return false;
    }

    pthread_attr_destroy(&attr);

    retval = pthread_mutex_init(&mStartLock, NULL);
    if (retval < 0) {
        LOGE("Failed to init enable mutex lock: %s", strerror(retval));
        return false;
    }

    retval = pthread_mutex_init(&mFileLock, NULL);
    if (retval < 0) {
        LOGE("Failed to init file mutex lock: %s", strerror(retval));
        return false;
    }

    retval = pthread_cond_init(&mStartCond, NULL);
    if (retval < 0) {
        LOGE("Failed to init condition: %s", strerror(retval));
        return false;
    }

    retval = setupResFile();
    if (retval < 0) {
        LOGE("Failed to setup resource file %s", strerror(retval));
        return false;
    }
//when system power on, shutdown voicetriggle
    enable(0);
    return true;
}

int VoiceTriggerSensor::setupResFile()
{
    int retval = 0;
    char buffer[10240] = {0};
    int resFd;
    int confFd;
    unsigned long fileSize = 0;
    struct stat sb;

    confFd = open(DEVICE_CONFIG_PATH, O_WRONLY | O_SYNC);
    if (confFd < 0) {
        LOGE("Failed to open %s: %s", DEVICE_CONFIG_PATH, strerror(errno));
        return false;
    }

    resFd = open(RESOURCE_PATH, O_RDONLY | O_SYNC);
    if (resFd < 0) {
        LOGE("Failed to open %s: %s", RESOURCE_PATH, strerror(errno));
        return false;
    }

    retval = stat(RESOURCE_PATH, &sb);
    if (retval < 0) {
        LOGE("Failed to read %s file size: %s", RESOURCE_PATH, strerror(retval));
        return false;
    } else {
        fileSize = sb.st_size;
    }

    while (fileSize > 0) {
        retval = read(resFd, buffer, sizeof(buffer));
        if (retval < 0) {
            LOGE("Failed to read %s: %s.", RESOURCE_PATH, strerror(errno));
            return retval;
        }

        retval = write(confFd, buffer, retval);
        if (retval < 0) {
            LOGE("Failed to write %s: %s.", DEVICE_CONFIG_PATH, strerror(errno));
            return retval;
        }

        fileSize -= retval;
    }

    close(resFd);
    close(confFd);

    return 0;
}

void *VoiceTriggerSensor::threadLoop(void *param)
{
    VoiceTriggerSensor* vs = reinterpret_cast<VoiceTriggerSensor *>(param);

    int count = 0;
    int retval = 0;
    char buf[32] = {0};
    int fd = 0;

    for (;;) {
        pthread_mutex_lock(&vs->mStartLock);
        while (!vs->mStartThread) {
            if (vs->mExit) {
                pthread_mutex_unlock(&vs->mStartLock);
                break;
            }
            pthread_cond_wait(&vs->mStartCond, &vs->mStartLock);
        }
        pthread_mutex_unlock(&vs->mStartLock);

        pthread_mutex_lock(&vs->mFileLock);
        fd = open(SYSFS_PATH, O_RDWR | O_SYNC);
        if (fd < 0)
            LOGW("Failed to open %s in threadLoop: %s", SYSFS_PATH, strerror(errno));
        pthread_mutex_unlock(&vs->mFileLock);

        /**
         * Note:
         * Block read, interface return "wakeup_ok" while write "0" to it.
         * The is unsync case, but mEnabled condition will prevent main thread
         * read event in this case
         */
        retval = read(fd, buf, sizeof(buf));
        if (retval > 0) {
            char msg = 'v';
            retval = write(vs->mWorkerWriteFd, &msg, 1);
            if (retval < 0) {
                LOGE("Failed to write msg to pipe: %s", strerror(errno));
                continue;
            }
       }

       pthread_mutex_lock(&vs->mFileLock);
       retval = close(fd);
       if (retval < 0)
           LOGW("Failed to close %s in threadLoop: %s", SYSFS_PATH, strerror(errno));
       pthread_mutex_unlock(&vs->mFileLock);
    }

    return NULL;
}

int VoiceTriggerSensor::readEvents(sensors_event_t* event, int count)
{
    if (!mEnabled || count <= 0) {
        return -EINVAL;
    }

    event->sensor = sSensor.handle;
    event->type = sSensor.type;
    event->data[0] = 1;

    LOGI("============>>> Wake Up <<<==============");

    return 1;
}

int VoiceTriggerSensor::populateSensorList(struct sensor_t* list) const
{
    memcpy(list, &sSensor, sizeof(struct sensor_t));
    return 1;
}

int VoiceTriggerSensor::enable(int enabled)
{
    int retval = 0;
    char buffer[2];
    int fd = 0;

    buffer[0] = enabled ? '1' : '0';
    buffer[1] = '\0';

    pthread_mutex_lock(&mFileLock);
    fd = open(SYSFS_PATH, O_RDWR | O_SYNC);
    if (fd < 0) {
        LOGE("Failed to open %s: %s", SYSFS_PATH, strerror(errno));
    }

    retval = write(fd, buffer, 2);
    if (retval < 0)
        LOGE("Failed to set %sable voice trigger sensor: %s", enabled ? "en" : "dis",
                strerror(retval));

    retval = close(fd);
    if (retval < 0) {
        LOGE("Failed to close %s: %s", SYSFS_PATH, strerror(errno));
    }
    pthread_mutex_unlock(&mFileLock);

    mEnabled = enabled ? true : false;

    pthread_mutex_lock(&mStartLock);
    mStartThread = mEnabled;
    pthread_cond_signal(&mStartCond);
    pthread_mutex_unlock(&mStartLock);

    LOGI("%s: set %sable", SENSOR_NAME, mEnabled ? "en" : "dis");

    return 0;
}

int VoiceTriggerSensor::setDelay(int64_t ns)
{
    LOGI("Ignore %s: set sample rate to : %lldms", SENSOR_NAME, NS_TO_MS(ns));

    return 0;
}

int VoiceTriggerSensor::getFd() const
{
    return mMainReadFd;
}

bool VoiceTriggerSensor::isExist() const
{
    return mExist;
}
