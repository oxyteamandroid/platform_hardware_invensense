/*
 * Copyright (C) 2008 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef ANDROID_GENERAL_SENSOR_MANAGER_H
#define ANDROID_GENERAL_SENSOR_MANAGER_H

#include <stdint.h>
#include <errno.h>
#include <sys/cdefs.h>
#include <sys/types.h>

#include "sensors.h"
#include "SensorBase.h"
#include "GeneralSensor.h"
#include "InputEventReader.h"
#include "sensor_params.h"

#define NULL_VALUE                                           (-100)

#define NS_TO_MS(n) (n / (1000 * 1000))
#define GET_ARRAY_LEN(array,len) {len = (sizeof(array) / sizeof(array[0]));}

/*****************************************************************************/

struct input_event;

class GeneralSensorManager {

public:
	enum {
		TEMPERATURE_SENSOR = 0,
		HUMIDITY_SENSOR,
		PRESSURE_SENSOR,
#ifndef SUPPORT_FOR_IWDS
		HEART_RATE_SENSOR,
#endif
		EKG_SENSOR,
		UV_SENSOR,
		STEP_COUNTER_SENSOR,
		PROXIMITY_SENSOR,
		PEDOMETER_SENSOR,
		GENERAL_SENSOR_NUM
	};

	GeneralSensorManager(struct sensor_t *list, int *num);
	~GeneralSensorManager();

    void scanGeneralSensors(struct sensor_t *list, int *num);
	int registerGeneralSensors(struct pollfd* fds, int* idtofd, int* startfd);

	GeneralSensor* scanGeneralDevice(const char* basePath, general_sensor_desc_t *supported, int length);
	static const char* getTAG(int type);

	int enable(int handle, int enabled);
	int setDelay(int handle, int64_t ns);
	int readGeneralSensorEvents(sensors_event_t *data, int count, int type);

	static general_sensor_desc_t *general_sensor_scan_map[GENERAL_SENSOR_NUM];

	static general_sensor_desc_t supported_temperature_sensors[];
	static general_sensor_desc_t supported_humidity_sensors[];
	static general_sensor_desc_t supported_pressure_sensors[];
	static general_sensor_desc_t supported_heart_rate_sensors[];
	static general_sensor_desc_t supported_ekg_sensors[];
	static general_sensor_desc_t supported_uv_sensors[];
	static general_sensor_desc_t supported_step_counter_sensors[];
	static general_sensor_desc_t supported_proximity_sensors[];
	static general_sensor_desc_t supported_pedometer_sensors[];

	static const char* basePaths[GENERAL_SENSOR_NUM];
	static int sensorCount[GENERAL_SENSOR_NUM];

protected:

	GeneralSensor *mGeneralSensors[GENERAL_SENSOR_NUM];
};

/*****************************************************************************/

#endif
