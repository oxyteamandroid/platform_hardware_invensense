#ifndef SENSOR_LISTENER_H
#define SENSOR_LISTENER_H

#include <android/sensor.h>
#include <gui/Sensor.h>
#include <gui/SensorManager.h>
#include <utils/Looper.h>

using namespace android;

class SensorLooperThread: public Thread {
public:
    SensorLooperThread(Looper* looper) :
            Thread(false) {
        mLooper = sp < Looper > (looper);
    }

    ~SensorLooperThread() {
        mLooper.clear();
    }

    virtual bool threadLoop() {
        int32_t ret = mLooper->pollOnce(-1);
        return true;
    }

    void wake() {
        mLooper->wake();
    }

private:
    sp<Looper> mLooper;
};

class SensorListener : public RefBase {
public:
    typedef enum {
        SENSOR_ACCELEROMETER = 1 << 0,
        SENSOR_MAGNETIC_FIELD = 1 << 1,
        SENSOR_GYROSCOPE = 1 << 2,
        SENSOR_LIGHT = 1 << 3,
        SENSOR_PROXIMITY = 1 << 4,
        SENSOR_ORIENTATION = 1 << 5,
    } sensor_type_t;

    sp<SensorEventQueue> mSensorEventQueue;

    SensorListener();
    virtual ~SensorListener();

    int initialize(void);
    int getRegisterSensorType(void);
    void setAccelerometer(float x, float y, float z);
    float* getAccelerometer();
    int enableSensor(sensor_type_t type);
    void disableSensor(sensor_type_t type);

private:
    float mAccel[3];
    int mSensorEnabled;
    int mRegisterSensorType;
    sp<Looper> mLooper;
    sp<SensorLooperThread> mSensorLooperThread;
    mutable Mutex mLock;
};

#endif //SENSOR_LISTENER_H
