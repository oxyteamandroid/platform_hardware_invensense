#ifndef VOICE_TRIGGER_SENSOR_H_
#define VOICE_TRIGGER_SENSOR_H_

#include <stdint.h>
#include <errno.h>
#include <sys/cdefs.h>
#include <sys/types.h>
#include <utils/RefBase.h>

#include "sensors.h"
#include "SensorBase.h"
#include "sensor_params.h"

#define SENSOR_NAME "Voice Trigger"
#define DEVICE_CONFIG_PATH "/dev/jz-wakeup"
#define RESOURCE_PATH "/etc/ivModel_v21.irf"
#define SYSFS_PATH "/sys/class/jz-wakeup/jz-wakeup/wakeup"

#define NS_TO_MS(n) (n / (1000 * 1000))

class VoiceTriggerSensor: public SensorBase {
public:
    VoiceTriggerSensor();
    virtual ~VoiceTriggerSensor();
    virtual int readEvents(sensors_event_t* event, int count);
    virtual int setDelay(int64_t ns);
    virtual int enable(int enabled);
    virtual int getFd() const;
    bool isExist() const;
    int populateSensorList(sensor_t* list) const;

private:
    static void *threadLoop(void *param);
    int setupResFile();
    bool scanSensor() const;
    bool initialize();
    int enableDisable(int enable);
    void msleep(long long msec) const;

    bool mExist;
    bool mEnabled;
    int mMainReadFd;
    int mWorkerWriteFd;
    bool mStartThread;
    bool mExit;
    pthread_mutex_t mFileLock;
    pthread_mutex_t mStartLock;
    pthread_cond_t mStartCond;
};
#endif /* VOICE_TRIGGER_SENSOR_H_ */
