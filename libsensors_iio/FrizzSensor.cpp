
#include <stdint.h>
#include <unistd.h>
#include <errno.h>
#include <sys/cdefs.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <pthread.h>
#include <hardware/sensors.h>
#include <new>

#include <poll.h>
#include <cutils/log.h>

#include "sensors.h"
#include "sensor_params.h"

#include "FrizzSensor.h"

#define INIT_SENSOR_LIST(n, h, fun, off, g, s)          \
    do { FrizzSensorList[n].handler = h;                \
        FrizzSensorList[n].func = fun;                  \
        FrizzSensorList[n].offset = off;                \
        FrizzSensorList[n].generals = g;                \
        FrizzSensorList[n].specials = s; } while (0)

#define pFRIZZe(fmt, args...) ALOGE(fmt, ##args)
#define pFRIZZi(fmt, args...) do{}while(0) // ALOGI(fmt, ##args)

#define FRIZZ_DEVICE_NAME "/dev/frizz"

static struct sensor_t sSensorList[] = {
    {"Accelerometer", "Frizz", 1,
    SENSORS_FRIZZ_ACC_HANDLE, SENSOR_TYPE_ACCELEROMETER,
    (4 * 9.8f), 0.001f, 1, 10000, 0, 0,
	SENSOR_STRING_TYPE_ACCELEROMETER,"",10000,SENSOR_FLAG_CONTINUOUS_MODE, { NULL }},

    {"Gyroscope", "Frizz", 1,
    SENSORS_FRIZZ_GYROSCOPE_HANDLE, SENSOR_TYPE_GYROSCOPE,
    9.0f, 0.001f, 1, 10000, 0, 0,
	SENSOR_STRING_TYPE_GYROSCOPE,"",10000,SENSOR_FLAG_CONTINUOUS_MODE, { NULL }},

    { "Gravity", "Frizz", 1,
    SENSORS_FRIZZ_GRAVITY_HANDLE, SENSOR_TYPE_GRAVITY,
    9.8f, 0.001f, 1, 10000, 0, 0,
	SENSOR_STRING_TYPE_GRAVITY,"",10000,SENSOR_FLAG_CONTINUOUS_MODE, { NULL }},

    { "Acceleration", "Frizz", 1,
    SENSORS_FRIZZ_LINEAR_ACCELERATION_HANDLE, SENSOR_TYPE_LINEAR_ACCELERATION,
    (3 * 9.8f), 0.001f, 1, 10000, 0, 0,
	SENSOR_STRING_TYPE_LINEAR_ACCELERATION,"",10000,SENSOR_FLAG_CONTINUOUS_MODE, { NULL }},

    {"Pressure", "Frizz", 1,
    SENSORS_FRIZZ_PRESSURE_HANDLE, SENSOR_TYPE_PRESSURE,
    1, 0.001f, 1, 10000, 0, 0,
	SENSOR_STRING_TYPE_PRESSURE,"",10000,SENSOR_FLAG_CONTINUOUS_MODE, { NULL }},

    {"StepDetector", "Frizz", 1,
    SENSORS_FRIZZ_STEP_DETECTOR_HANDLE, SENSOR_TYPE_STEP_DETECTOR,
    1, 1, 1, 0, 0, 0,
	SENSOR_STRING_TYPE_STEP_DETECTOR,"",10000,SENSOR_FLAG_CONTINUOUS_MODE, { NULL }},

    {"StepCounter", "Frizz", 1,
    SENSORS_FRIZZ_STEP_COUNTER_HANDLE, SENSOR_TYPE_STEP_COUNTER,
    100, 1, 1, 0, 0, 0,
	SENSOR_STRING_TYPE_STEP_COUNTER,"",10000,SENSOR_FLAG_CONTINUOUS_MODE, { NULL }},

    {"PDRStepCounter", "Frizz", 1,
    SENSORS_FRIZZ_PDR_STEP_COUNTER_HANDLE, SENSOR_TYPE_STEP_COUNTER,
    100, 1, 1, 0, 0, 0,
	SENSOR_STRING_TYPE_STEP_COUNTER,"",10000,SENSOR_FLAG_CONTINUOUS_MODE, { NULL }},

    {"Magnetometer", "Frizz", 1,
    SENSORS_FRIZZ_GEOMAGNETIC, SENSOR_TYPE_GEOMAGNETIC_FIELD,
    4900, 0.15f, 1, 10000, 0, 0,
	SENSOR_STRING_TYPE_MAGNETIC_FIELD,"",10000,SENSOR_FLAG_CONTINUOUS_MODE, { NULL }},

    {"Light", "Frizz", 1,
    SENSORS_FRIZZ_LIGHT, SENSOR_TYPE_LIGHT,
    100, 1, 1, 10000, 0, 0,
	SENSOR_STRING_TYPE_LIGHT,"",10000,SENSOR_FLAG_CONTINUOUS_MODE, { NULL }},

    {"Proximity", "Frizz", 1,
    SENSORS_FRIZZ_PROXIMITY, SENSOR_TYPE_PROXIMITY,
    100, 1, 1, 10000, 0, 0,
	SENSOR_STRING_TYPE_PROXIMITY,"",10000,SENSOR_FLAG_CONTINUOUS_MODE, { NULL }},

    {"Gesture", "Frizz", 1,
    SENSORS_FRIZZ_GESTURE, SENSOR_TYPE_GESTURE,
    100, 1, 1, 10000, 0, 0,
	SENSOR_STRING_TYPE_GESTURE,"",10000,SENSOR_FLAG_CONTINUOUS_MODE, { NULL }},

    {"Motion", "Frizz", 1,
    SENSORS_FRIZZ_MOTION, SENSOR_TYPE_MOTION,
    100, 1, 1, 10000, 0, 0,
	SENSOR_STRING_TYPE_MOTION,"",10000,SENSOR_FLAG_CONTINUOUS_MODE, { NULL }},

    {"Orientation", "Frizz", 1,
    SENSORS_FRIZZ_ORIENTATION, SENSOR_TYPE_ORIENTATION,
    360, 0.001f, 1, 10000, 0, 0,
	SENSOR_STRING_TYPE_ORIENTATION,"",10000,SENSOR_FLAG_CONTINUOUS_MODE, { NULL }},

};

int FrizzSensor::initSensorList(void)
{
    memset(FrizzSensorList, 0, sizeof(frizz_sensor_t) * SENSORS_ID_FRIZZ_NUM);

    INIT_SENSOR_LIST(0, SENSORS_FRIZZ_ACC_HANDLE,
            &FrizzSensor::hw_readAccelerometerHandler,
            FRIZZ_OFFSET_ACCELEROMETER, true, false);

    INIT_SENSOR_LIST(1, SENSORS_FRIZZ_GYROSCOPE_HANDLE,
            &FrizzSensor::hw_readGyroscopeHandler,
            FRIZZ_OFFSET_GYROSCOPE, true, false);

    INIT_SENSOR_LIST(2, SENSORS_FRIZZ_GRAVITY_HANDLE,
            &FrizzSensor::hw_readGravityHandler,
            FRIZZ_OFFSET_GRAVITY, true, false);

    INIT_SENSOR_LIST(3, SENSORS_FRIZZ_LINEAR_ACCELERATION_HANDLE,
            &FrizzSensor::hw_readLinearAccHandler,
            FRIZZ_OFFSET_LINEAR_ACCELERATION, true, false);

    INIT_SENSOR_LIST(4, SENSORS_FRIZZ_PRESSURE_HANDLE,
            &FrizzSensor::hw_readPressureHandler,
            FRIZZ_OFFSET_PRESSURE, true, true);

    INIT_SENSOR_LIST(5, SENSORS_FRIZZ_STEP_DETECTOR_HANDLE,
            &FrizzSensor::hw_readStepDetectorHandler,
            FRIZZ_OFFSET_STEP_DETECTOR, false, false);

    INIT_SENSOR_LIST(6, SENSORS_FRIZZ_STEP_COUNTER_HANDLE,
            &FrizzSensor::hw_readStepCounterHandler,
            FRIZZ_OFFSET_STEP_COUNTER, true, true);

    INIT_SENSOR_LIST(7, SENSORS_FRIZZ_PDR_STEP_COUNTER_HANDLE,
            &FrizzSensor::hw_readPDRStepCounterHandler,
            FRIZZ_OFFSET_PDR, false, false);

    INIT_SENSOR_LIST(8, SENSORS_FRIZZ_GEOMAGNETIC,
            &FrizzSensor::hw_readMagneticFieldHandler,
            FRIZZ_OFFSET_MAGNETIC_FIELD, true, false);

    INIT_SENSOR_LIST(9, SENSORS_FRIZZ_LIGHT,
            &FrizzSensor::hw_readLigthHandler,
            FRIZZ_OFFSET_LIGHT, true, false);

    INIT_SENSOR_LIST(10, SENSORS_FRIZZ_PROXIMITY,
            &FrizzSensor::hw_readProximityHandler,
            FRIZZ_OFFSET_PROXIMITY, true, false);

    INIT_SENSOR_LIST(11, SENSORS_FRIZZ_GESTURE,
            &FrizzSensor::hw_readGestureHandler,
            FRIZZ_OFFSET_GESTURE, true, true);

    INIT_SENSOR_LIST(12, SENSORS_FRIZZ_MOTION,
            &FrizzSensor::hw_readMotineHandler,
            FRIZZ_OFFSET_ISP, false, false);

    INIT_SENSOR_LIST(13, SENSORS_FRIZZ_ORIENTATION,
            &FrizzSensor::hw_readOrientationHandler,
            FRIZZ_OFFSET_ORIENTATION, true, false);

    int nSensorList = ARRAY_SIZE(sSensorList);

    for (int i = 0; i < SENSORS_ID_FRIZZ_NUM; i++) {
        for (int j = 0; j < nSensorList; j ++) {
            if (sSensorList[j].handle == FrizzSensorList[i].handler) {
                FrizzSensorList[i].list = &sSensorList[j];
                pFRIZZe("push frizz handle %d to SensorList: %s",
                        sSensorList[j].handle, sSensorList[j].name);
            }
        }
    }

    return 0;
}

//FrizzSensor::FrizzSensor(frizz_hal_t type): SensorBase(NULL, NULL)
//{
//    halType = type;
//    new(this) FrizzSensor();
//}

FrizzSensor::FrizzSensor(frizz_hal_t type) : SensorBase(FRIZZ_DEVICE_NAME, NULL)
{
    int ret = open_device();
    if (ret < 0)
        pFRIZZe("Frizz device does not open %s", FRIZZ_DEVICE_NAME);
    else {

        halType = type;

        int vers = hw_getversion();
        pFRIZZe("Frizz device firmware version %d", vers);

        int gPipe[2];
        int tPipe[2];

        ret = pipe(gPipe);
        if (ret < 0)
            pFRIZZe("creating GENERAL SENSOR pipe (%s)", strerror(errno));

        ret = pipe(tPipe);
        if (ret < 0)
            pFRIZZe("creating wakeup thread pipe (%s)", strerror(errno));

        gpoll = gPipe[0];
        gwake = gPipe[1];

        tpoll = tPipe[0];
        twake = tPipe[1];

        pFRIZZi("Frizz sereor pipe p:%d w:%d",gpoll, gwake);
        pFRIZZi("Frizz thread pipe p:%d w:%d",tpoll, twake);

        initSensorList();

        create_thread();
    }
}

FrizzSensor::~FrizzSensor()
{
    cancal_thread();

    close(gpoll);
    close(gwake);

    close(tpoll);
    close(twake);
}

#define POLL_NUM_FD 2

void *FrizzSensor::poll_thread(void *arg)
{
    FrizzSensor *fs = reinterpret_cast<FrizzSensor *> (arg);

    struct pollfd pfs[POLL_NUM_FD];

    pfs[0].fd = fs->dev_fd;
    pfs[0].events = POLLIN;
    pfs[0].revents = 0;

    pfs[1].fd = fs->tpoll;
    pfs[1].events = POLLIN;
    pfs[1].revents = 0;

    pFRIZZe("Frizz poll_thread Running");

    while (fs->thread_run) {
        int n = 0;
        do {
            n = poll(pfs, POLL_NUM_FD, -1);
        } while (n < 0 && errno == EINTR);

        if (n < 0) {
            pFRIZZe("Frizz poll_thread fd error quit");
            continue;
        }

        if (pfs[0].revents & POLLIN) {
            pfs[0].revents = 0;
            pFRIZZi("Frizz poll_thread sync_event");
            fs->sync_event();
        }

        if (pfs[1].revents & POLLIN) {
            pfs[1].revents = 0;
            char msg('w');
            int result = read(pfs[1].fd, &msg, 1);
            ALOGE_IF(result < 0, "error reading from wake pipe (%s)",
                    strerror(errno));
        }
    }

    return NULL;
}

struct FrizzSensor::frizz_sensor_t *FrizzSensor::getSensorList(int handle)
{
    struct frizz_sensor_t *p = FrizzSensorList;

    for (int i = 0; i < SENSORS_ID_FRIZZ_NUM; i++) {
        if (p[i].handler == handle) {
            if ((halType == FRIZZ_GENERAL_HAL && p[i].generals)
                    || (halType == FRIZZ_SPECIAL_HAL && p[i].specials))
                return &p[i];
        }
    }

    return NULL;
}

int FrizzSensor::getSensorId(sensor_t *p)
{
    return 0;
}

int FrizzSensor::create_thread(void)
{
    pthread_t tid;
    pthread_attr_t attr;
    pthread_attr_init(&attr);
    pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_DETACHED);

    thread_run = true;
    int ret = pthread_create(&tid, &attr, poll_thread, this);
    if (ret) {
        pFRIZZe("failed to create thread %s", strerror(ret));
        return -1;
    }

    pthread_attr_destroy(&attr);

    return 0;
}

int FrizzSensor::cancal_thread(void)
{
    thread_run = false;
    char msg('w');
    int result = write(twake, &msg, 1);
    ALOGE_IF(result < 0, "error sending wake message (%s)", strerror(errno));
    return 0;
}

int FrizzSensor::sync_event(void)
{
    char msg('w');
    int result = write(gwake, &msg, 1);
    ALOGE_IF(result < 0, "error sending wake general message (%s)",
            strerror(errno));
    return result;
}

int FrizzSensor::activate(int handle, int enabled)
{
	int retm, rets;
    sensor_enable_t sensor_enable;

    memset(&sensor_enable, 0x00, sizeof(sensor_enable_t));

    struct frizz_sensor_t *s = getSensorList(handle);

    if (s == NULL)
        return -1;

//特殊：睡眠检测算法跟运动（motion）算法在上层看起来就只有motion,
//所以使能/关闭 都要相应的使能/关闭 睡眠检测
//跌倒检测也是属于motion 类
    if(s->offset == FRIZZ_OFFSET_ISP) {
        sensor_enable.type = FRIZZ_OFFSET_ACTIVITY;
        sensor_enable.flag = enabled;
        rets = hw_updateEnable(&sensor_enable);

        sensor_enable.type = FRIZZ_OFFSET_ACCEL_POS_DET;
		sensor_enable.flag = enabled;
        rets = hw_updateEnable(&sensor_enable);
    }
//结束
    sensor_enable.type = s->offset;
    sensor_enable.flag = enabled;

    pFRIZZi("FrizzSensor::activate %d e:%d t:%d", handle, enabled,
            sensor_enable.type);

    retm =  hw_updateEnable(&sensor_enable);
	return (!!rets) && (!!retm);
}

int FrizzSensor::setDelay(int handle, int64_t ns)
{
    pFRIZZi("FrizzSensor::setDelay %d %d", handle, ns);

    struct frizz_sensor_t *s = getSensorList(handle);
    if (s == NULL)
        return -1;

    sensor_delay_t sensor_delay;
    memset(&sensor_delay, 0x00, sizeof(sensor_delay_t));
    sensor_delay.type = s->offset;
    sensor_delay.ms = ns / 1000000;

    return hw_updateDelay(&sensor_delay);
}

int FrizzSensor::setRightHand(int handle, int isRightHand)
{
    pFRIZZi("FrizzSensor::setRightHand %d %d", handle, isRightHand);

    int fd;
    const char *SET_RIGHT_HAND_PATH = "/sys/frizz/set_right_hand_wear";
    const char *buf = (isRightHand != 0) ? "1" : "0";

    fd = open(SET_RIGHT_HAND_PATH, O_WRONLY);
    if (fd < 0) {
        pFRIZZe("FrizzSensor::setRightHand opening sys failed");
        return -1;
    }

    if (write(fd, buf, strlen(buf)) < 0) {
        pFRIZZe("FrizzSensor::setRightHand writting sys failed");
        close(fd);
        return -1;
    }

    close(fd);

    return 0;
}

int FrizzSensor::getFd(void)
{
    return gpoll;
}

bool FrizzSensor::isEnabled(void)
{
    pFRIZZi("FrizzSensor::isEnabled\n/dev/frizz fd:%d", dev_fd);
    if( hw_getversion() == 0)
        return false;

    return (dev_fd > 0);
}

int FrizzSensor::readEvents(sensors_event_t* data, int count)
{
    // see if we have some leftover from the last poll()
    if (data == NULL || count < 1) {
        return -EINVAL;
    }

    int numEventReceived = 0;

    sensors_event_t event;
    sensor_data_changed_t change;
    hw_getchange(&change);

    /* if data is empty, system bug */
    data[0].type = SENSOR_TYPE_ACCELEROMETER;
    data[0].version = sizeof(sensors_event_t);
    data[0].sensor = ID_FRIZZ_ACC;

    pFRIZZi("Sensor data Change data %x %x", change.updateflag1,
            change.updateflag2);
//特殊：睡眠检测 与 运动识别 在上层看来就只有运动识别，但运动识别包含睡眠检测
//所以如果睡眠检测有相应上报，需要特殊处理上报成一次 运动识别
//跌倒检测 也属于motion 类
    if((change.updateflag2 & (1 << (FRIZZ_OFFSET_ACTIVITY - 31))) && (halType == FRIZZ_SPECIAL_HAL)) {
        if(hw_readSpecialSensorEvent_activity(&event, FRIZZ_OFFSET_ACTIVITY)) {
			*data++ = event;
			count--;
			numEventReceived++;
		}
    }
	if((change.updateflag2 & (1 << (FRIZZ_OFFSET_ACCEL_POS_DET - 31))) && (halType == FRIZZ_SPECIAL_HAL)) {
		if(hw_readSpecialSensorEvent_activity(&event, FRIZZ_OFFSET_ACCEL_POS_DET)) {
			*data++ = event;
			count--;
			numEventReceived++;
		}
	}
//结束

    for (int i = 0; i < SENSORS_ID_FRIZZ_NUM; i++) {
        uint32_t check;

        int offset = FrizzSensorList[i].offset;

        if (offset > 31) {
            offset -= 31;
            check = change.updateflag2;
        } else
            check = change.updateflag1;

        if (!(check & (1 << offset))) {
            continue;
        }

        hfunc_t mHandlers = FrizzSensorList[i].func;
        memset(&event, 0 , sizeof(sensors_event_t));
        int update = (this->*mHandlers)(&event);

        if (halType == FRIZZ_GENERAL_HAL && FrizzSensorList[i].generals && update
                && (count > 0)) {
            *data++ = event;
            count--;
            numEventReceived++;
        } else if (halType == FRIZZ_SPECIAL_HAL && FrizzSensorList[i].specials
                && update && (count > 0)) {
            *data++ = event;
            count--;
            numEventReceived++;
        }
    }

    return numEventReceived;
}

int FrizzSensor::populateSensorList(struct sensor_t *list)
{
    int count = 0;

    for (int i = 0; i < ARRAY_SIZE(FrizzSensorList); i++) {

        if (activate(FrizzSensorList[i].handler, 1))
            continue;

        if (halType == FRIZZ_GENERAL_HAL && FrizzSensorList[i].generals)
            memcpy(&list[count++], &sSensorList[i], sizeof(struct sensor_t));

        if (halType == FRIZZ_SPECIAL_HAL && FrizzSensorList[i].specials)
            memcpy(&list[count++], &sSensorList[i], sizeof(struct sensor_t));

        activate(FrizzSensorList[i].handler, 0);
    }

    return count;
}

int FrizzSensor::hw_readSensorEvent(sensor_data_t *event, int type)
{
    event->code = type;
    int err = ioctl(dev_fd, FRIZZ_IOCTL_SENSOR_GET_DATA, event);
    ALOGE_IF(err, "type = %d failed (%s)", type, strerror(-err));
    return err;
}

uint32_t FrizzSensor::hw_getchange(sensor_data_changed_t *change)
{
    memset(change, 0, sizeof(sensor_data_changed_t));

    ioctl(dev_fd, FRIZZ_IOCTL_GET_SENSOR_DATA_CHANGED, change);

    return 0;
}

int FrizzSensor::hw_updateEnable(sensor_enable_t *sensor_enable)
{
    int err = ioctl(dev_fd, FRIZZ_IOCTL_SENSOR_SET_ENABLE, sensor_enable);
    ALOGE_IF(err, "updateEnable type = %d failed (%s)", sensor_enable->type,
            strerror(-err));
    return err;
}

uint32_t FrizzSensor::hw_getversion()
{
    firmware_version_t v;
    v.version = 0;

    int err = ioctl(dev_fd, FRIZZ_IOCTL_GET_FIRMWARE_VERSION, &v);
    ALOGE_IF(err, "frizz getversion failed (%s)", strerror(-err));

    return v.version;
}

int FrizzSensor::hw_updateDelay(sensor_delay_t *delay)
{
    int err = 0;
    err = ioctl(dev_fd, FRIZZ_IOCTL_SENSOR_SET_DELAY, delay);
    ALOGE_IF(err, "updateDelay type = %d failed (%s)", delay->type,
            strerror(-err));
    return err;
}

uint32_t FrizzSensor::hw_updateFirmware(const char *path)
{
    // firmware download "/data/frizz/from.bin"
    int vers = hw_getversion();
    if (vers == 0) {
        ioctl(dev_fd, FRIZZ_IOCTL_HARDWARE_DOWNLOAD_FIRMWARE, path);
        vers = hw_getversion();
    }

    pFRIZZi("Frizz update firmware version %d", vers);

    return 0;
}

int FrizzSensor::hw_readAccelerometerHandler(sensors_event_t *data)
{
    int result = 0;
    sensor_data_t sensor;

    data[0].type = SENSOR_TYPE_ACCELEROMETER;
    data[0].version = sizeof(sensors_event_t);
    data[0].sensor = ID_FRIZZ_ACC;

    memset(&sensor, 0x0, sizeof(sensor));
    int err = hw_readSensorEvent(&sensor, data[0].type);
    if (!err) {
        data->acceleration.x = sensor.f32_value[0] * 9.8; // x
        data->acceleration.y = sensor.f32_value[1] * 9.8; // y
        data->acceleration.z = sensor.f32_value[2] * 9.8; // z
        data->timestamp = timevalToNano(sensor.time);
        result = 1;
    }

    return result;
}

int FrizzSensor::hw_readPressureHandler(sensors_event_t *data)
{
    int result = 0;
    sensor_data_t sensor;
    memset(&sensor, 0x00, sizeof(sensor));

    data[0].type = SENSOR_TYPE_PRESSURE;
    data[0].version = sizeof(sensors_event_t);
    data[0].sensor = ID_FRIZZ_PRESSURE;

    int err = hw_readSensorEvent(&sensor, data[0].type);
    if (!err) {
        data->pressure = sensor.f32_value[0];
        data->timestamp = timevalToNano(sensor.time);
        result = 1;
    }

    return result;
}

int FrizzSensor::hw_readGyroscopeHandler(sensors_event_t *data)
{
    int result = 0;
    sensor_data_t sensor;

    data[0].type = SENSOR_TYPE_GYROSCOPE;
    data[0].version = sizeof(sensors_event_t);
    data[0].sensor = ID_FRIZZ_GYROSCOPE;

    memset(&sensor, 0x00, sizeof(sensor));
    int err = hw_readSensorEvent(&sensor, data[0].type);
    if (!err) {
        data->gyro.x = sensor.f32_value[0]; // x
        data->gyro.y = sensor.f32_value[1]; // y
        data->gyro.z = sensor.f32_value[2]; // z
        data->timestamp = timevalToNano(sensor.time);
        result = 1;
    }

    return result;
}

int FrizzSensor::hw_readGravityHandler(sensors_event_t *data)
{
    int result = 0;
    sensor_data_t sensor;

    data[0].type = SENSOR_TYPE_GRAVITY;
    data[0].version = sizeof(sensors_event_t);
    data[0].sensor = ID_FRIZZ_GRAVITY;

    memset(&sensor, 0x00, sizeof(sensor));
    int err = hw_readSensorEvent(&sensor, data[0].type);
    if(!err){
        data->acceleration.x = sensor.f32_value[0]; // x
        data->acceleration.y = sensor.f32_value[1]; // y
        data->acceleration.z = sensor.f32_value[2]; // z
        data->timestamp = timevalToNano(sensor.time);
        result = 1;
    }

    return result;
}

int FrizzSensor::hw_readLinearAccHandler(sensors_event_t *data)
{
    int result = 0;
    sensor_data_t sensor;

    data[0].type = SENSOR_TYPE_LINEAR_ACCELERATION;
    data[0].version = sizeof(sensors_event_t);
    data[0].sensor = ID_FRIZZ_LINEAR_ACCELERATION;

    memset(&sensor, 0x00, sizeof(sensor));
    int err = hw_readSensorEvent(&sensor, data[0].type);
    if(!err){
        data->acceleration.x = sensor.f32_value[0]; // x
        data->acceleration.y = sensor.f32_value[1]; // y
        data->acceleration.z = sensor.f32_value[2]; // z
        data->timestamp = timevalToNano(sensor.time);
        result = 1;
    }

    return result;
}

int FrizzSensor::hw_readStepDetectorHandler(sensors_event_t *data)
{
    int result = 0;
    sensor_data_t sensor;

    data[0].type = SENSOR_TYPE_STEP_DETECTOR;
    data[0].version = sizeof(sensors_event_t);
    data[0].sensor = ID_FRIZZ_STEP_DETECTOR;

    memset(&sensor, 0x00, sizeof(sensor));
    int err = hw_readSensorEvent(&sensor, data[0].type);
    if(!err){
        int64_t time = timevalToNano(sensor.time);
        if (time != mStepSensorTimestamp) {
            data->timestamp = mStepSensorTimestamp = time;
            data->data[0] = 1;
            result = 1;
        }
    }

    return result;
}

int FrizzSensor::hw_readStepCounterHandler(sensors_event_t *data)
{
    int result = 0;
    sensor_data_t sensor;

    data[0].type = SENSOR_TYPE_STEP_COUNTER;
    data[0].version = sizeof(sensors_event_t);
    data[0].sensor = ID_FRIZZ_STEP_COUNTER;

    memset(&sensor, 0x00, sizeof(sensor));
    int err = hw_readSensorEvent(&sensor, data[0].type);
    if(!err && (sensor.u32_value != mLastStepCount)){
        mLastStepCount = sensor.u32_value;
        data->data[0] = (float)sensor.u32_value;
        data->timestamp = timevalToNano(sensor.time);
        result = 1;
    }

    return result;
}

int FrizzSensor::hw_readPDRStepCounterHandler(sensors_event_t *data)
{
    int result = 0;
    sensor_data_t sensor;

    data[0].type = SENSOR_TYPE_STEP_COUNTER;
    data[0].version = sizeof(sensors_event_t);
    data[0].sensor = ID_FRIZZ_PDR_STEP_COUNTER;

    memset(&sensor, 0x00, sizeof(sensor));
    int err = hw_readSensorEvent(&sensor, FRIZZ_OFFSET_PDR);
    if(!err && (sensor.count != mLastPDRStepCount)){
        mLastPDRStepCount = sensor.count;
        data->data[0] = (float)sensor.count;
        data->timestamp = timevalToNano(sensor.time);
        result = 1;
    }

    return result;
}

int FrizzSensor::hw_readMagneticFieldHandler(sensors_event_t *data)
{
    int result = 0;
    sensor_data_t sensor;

    data[0].type = SENSOR_TYPE_GEOMAGNETIC_FIELD;
    data[0].version = sizeof(sensors_event_t);
    data[0].sensor = ID_FRIZZ_GEOMAGNETIC;

    memset(&sensor, 0x00, sizeof(sensor));
    int err = hw_readSensorEvent(&sensor, data[0].type);
    if(!err) {
        data->magnetic.x = sensor.f32_value[0]; // x
        data->magnetic.y = sensor.f32_value[1]; // y
        data->magnetic.z = sensor.f32_value[2]; // z
        data->timestamp = timevalToNano(sensor.time);
        result = 1;
    }

    return result;
}

int FrizzSensor::hw_readLigthHandler(sensors_event_t *data)
{
    int result = 0;
    sensor_data_t sensor;
    memset(&sensor, 0x00, sizeof(sensor));

    data[0].type = SENSOR_TYPE_LIGHT;
    data[0].version = sizeof(sensors_event_t);
    data[0].sensor = ID_FRIZZ_LIGHT;

    int err = hw_readSensorEvent(&sensor, data[0].type);
    if (!err) {
        data->light = sensor.f32_value[0];
        data->timestamp = timevalToNano(sensor.time);
        result = 1;
    }

    return result;
}

int FrizzSensor::hw_readProximityHandler(sensors_event_t *data)
{
    int result = 0;
    sensor_data_t sensor;
    memset(&sensor, 0x00, sizeof(sensor));

    data[0].type = SENSOR_TYPE_PROXIMITY;
    data[0].version = sizeof(sensors_event_t);
    data[0].sensor = ID_FRIZZ_PROXIMITY;

    int err = hw_readSensorEvent(&sensor, data[0].type);
    if (!err) {
        data->light = sensor.f32_value[0];
        data->timestamp = timevalToNano(sensor.time);
        result = 1;
    }

    return result;
}

int FrizzSensor::hw_readGestureHandler(sensors_event_t *data)
{
    int result = 0;
    sensor_data_t sensor;
    memset(&sensor, 0x00, sizeof(sensor));

    data[0].type = SENSOR_TYPE_GESTURE;
    data[0].version = sizeof(sensors_event_t);
    data[0].sensor = ID_FRIZZ_GESTURE;

    int err = hw_readSensorEvent(&sensor, FRIZZ_OFFSET_GESTURE);
    if (!err) {
        data->data[0] = sensor.u32_value;
        data->timestamp = timevalToNano(sensor.time);
        result = 1;
    }

    return result;
}

int FrizzSensor::hw_readMotineHandler(sensors_event_t *data)
{
    int result = 0;
    sensor_data_t sensor;
    memset(&sensor, 0x00, sizeof(sensor));

    data[0].type = SENSOR_TYPE_MOTION;
    data[0].version = sizeof(sensors_event_t);
    data[0].sensor = ID_FRIZZ_MOTION;

    int err = hw_readSensorEvent(&sensor, FRIZZ_OFFSET_ISP);
    if (!err) {
        data->data[0] = sensor.u32_value;
        data->timestamp = timevalToNano(sensor.time);
        result = 1;
    }

    return result;
}
int FrizzSensor::hw_readSpecialSensorEvent_activity(sensors_event_t* data, int sensor_offset)
{
    int result = 0;
    sensor_data_t sensor;
    memset(&sensor, 0x00, sizeof(sensor));

    data[0].type = SENSOR_TYPE_MOTION;
    data[0].version = sizeof(sensors_event_t);
    data[0].sensor = ID_FRIZZ_MOTION;
    int err = hw_readSensorEvent(&sensor, sensor_offset);
    if (!err) {
        data->data[0] = sensor.u32_value;
        data->timestamp = timevalToNano(sensor.time);
        result = 1;
    }

    return result;
}

int FrizzSensor::hw_readOrientationHandler(sensors_event_t *data)
{
    int result = 0;
    sensor_data_t sensor;
    memset(&sensor, 0x00, sizeof(sensor));

    data[0].type = SENSOR_TYPE_ORIENTATION;
    data[0].version = sizeof(sensors_event_t);
    data[0].sensor = ID_FRIZZ_ORIENTATION;

    int err = hw_readSensorEvent(&sensor, data[0].type);
    if (!err) {
        data->orientation.x = sensor.f32_value[0]; // azimuth
        data->orientation.y = sensor.f32_value[1]; // pitch
        data->orientation.z = sensor.f32_value[2]; // roll
        data->timestamp = timevalToNano(sensor.time);
        result = 1;
    }

    return result;
}



