/*
 * Copyright (C) 2012 Invensense, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#define LOG_NDEBUG 0

#include <fcntl.h>
#include <errno.h>
#include <math.h>
#include <float.h>
#include <poll.h>
#include <unistd.h>
#include <dirent.h>
#include <stdlib.h>
#include <sys/select.h>
#include <dlfcn.h>
#include <pthread.h>

#include <cutils/log.h>
#include <utils/KeyedVector.h>

#include "invensense.h"

#include "MPLSensorSysPed.h"
#include "MPLSupport.h"
#include "ml_sysfs_helper.h"

#define MAX_SYSFS_ATTRB (sizeof(struct pedo_sysfs_attrbs) / sizeof(char *))


MplSysPed_Interface* getSysPedInterfaceObject()
{
    MPLSensorSysPed* s = static_cast<MPLSensorSysPed*>(MPLSensor::gMPLSensor);
    return static_cast<MplSysPed_Interface*>(s);
}


/**************** Invensense Library interactions  ****************************/

MPLSensorSysPed::MPLSensorSysPed(CompassSensor *compass)
                    : MPLSensor(compass),
                      mCachedSteps(0),
                      mCachedWalktime(0)
{
    inv_init_pedo_sysfs_attributes();
}

MPLSensorSysPed::~MPLSensorSysPed()
{
    /* do nothing */
}

/****************** RPC interface implementation ******************************/

MPLSensorSysPed::MplSysPed_Interface::~MplSysPed_Interface()
{
    /* do nothing */
}

int MPLSensorSysPed::rpcStartPed()
{
    VFUNC_LOG;

    LOGI("HAL:pedometer - start");

    pthread_mutex_lock(&mHALMutex);

	if (mDmpPedometerEnabled) {
        LOGI("HAL:pedometer - already running... do nothing");
		pthread_mutex_unlock(&mHALMutex);
        return INV_SUCCESS;
    }
    LOGV_IF(EXTRA_VERBOSE, 
            "HAL:pedometer - mCachedSteps=%d mCachedWalktime=%lld",
            mCachedSteps, mCachedWalktime);

	onPower(1);

	enableDmpPedometer(1);

    inv_clear_dmp_pedo_data(); 
	inv_set_pedo_parameters();

	update_delay();

	pthread_mutex_unlock(&mHALMutex);
    return INV_SUCCESS;
}

int MPLSensorSysPed::rpcStopPed()
{
    VFUNC_LOG;

    int idata;
    long long lldata;
    int ret;

    pthread_mutex_lock(&mHALMutex);

    LOGI("HAL:pedometer - stop");

	if (!mDmpPedometerEnabled) {
        LOGI("HAL:pedometer - not running... do nothing");
		pthread_mutex_unlock(&mHALMutex);
        return INV_SUCCESS;
    }

    /* Cache DMP data */
    ret = inv_get_dmp_pedo_steps(&idata);
    LOGE_IF(ret, "HAL:pedometer - failed to cache steps ret=%d", ret);
    mCachedSteps += idata;
    if (mCachedSteps < 0)
		inv_clear_pedo_data();

    ret = inv_get_dmp_pedo_walktime(&lldata);
    LOGE_IF(ret, "HAL:pedometer - failed to cache walk time ret=%d", ret);
    mCachedWalktime += lldata;
    /* protect from storage overflow and representation of overflow, 
       respectively at the 63 bits and 31 bit boundary */
    if (mCachedWalktime < 0 || mCachedWalktime >= (1LL << 31))
		inv_clear_pedo_data();

    LOGV_IF(EXTRA_VERBOSE,
            "HAL:pedometer - mCachedSteps=%d mCachedWalktime=%lld", 
            mCachedSteps, mCachedWalktime);

	enableDmpPedometer(0);
	update_delay();

	pthread_mutex_unlock(&mHALMutex);
    return INV_SUCCESS;
}

int MPLSensorSysPed::rpcGetSteps()
{
    VFUNC_LOG;

    int data;
    int ret;

    ret = inv_get_dmp_pedo_steps(&data);
    data += mCachedSteps;
    if (data < 0) {
		pthread_mutex_lock(&mHALMutex);
		inv_clear_pedo_data();
		pthread_mutex_unlock(&mHALMutex);
        data = 0;
    }

    return data;
}

long MPLSensorSysPed::rpcGetWalkTime()
{
    VFUNC_LOG;

    int ret;
    long long lldata;

    ret = inv_get_dmp_pedo_walktime(&lldata);
    lldata += mCachedWalktime;

    /* protect from storage overflow and representation of overflow, 
       respectively at the 63 bits and 31 bit boundary */
    if (lldata < 0 || lldata >= (1LL << 31)) {
		pthread_mutex_lock(&mHALMutex);
		inv_clear_pedo_data();
		pthread_mutex_unlock(&mHALMutex);
        lldata = 0;
    }

    return (long)lldata;
}

int MPLSensorSysPed::rpcClearPedData()
{
    VFUNC_LOG;

	pthread_mutex_lock(&mHALMutex);
    inv_clear_pedo_data(); 
	pthread_mutex_unlock(&mHALMutex);

    return INV_SUCCESS;
}


/****************** Internal logic implementation *****************************/
int MPLSensorSysPed::inv_clear_pedo_data(void)
{
    inv_clear_dmp_pedo_data(); 
    mCachedSteps = 0;
    mCachedWalktime = 0;

    return INV_SUCCESS;
}

int MPLSensorSysPed::inv_clear_dmp_pedo_data(void)
{
    VFUNC_LOG;

    LOGI("HAL:pedometer - reset");

	if (mEnabled || mDmpOrientationEnabled || mDmpPedometerEnabled) 
		masterEnable(0);

    if (write_sysfs_int(mpu_pedo.steps, 0) < 0) 
		LOGE("HAL:ERR can't write pedometer steps");
    if (write_sysfs_longlong(mpu_pedo.walk_time, 0) < 0)
		LOGE("HAL:ERR can't write pedometer walk time");

	if (mEnabled || mDmpOrientationEnabled || mDmpPedometerEnabled) 
		masterEnable(1);

    return INV_SUCCESS;
}

int MPLSensorSysPed::inv_set_pedo_parameters(void)
{
    VFUNC_LOG;

	if (mEnabled || mDmpOrientationEnabled || mDmpPedometerEnabled) 
		masterEnable(0);

	write_sysfs_int(mpu_pedo.clip_threshold, 0x04000000L);
	write_sysfs_int(mpu_pedo.min_steps, 5L);
	write_sysfs_int(mpu_pedo.max_step_buffer_time, 150L);
	write_sysfs_int(mpu_pedo.threshold, 25000000L);
	write_sysfs_int(mpu_pedo.min_up_time, 16L);
	write_sysfs_int(mpu_pedo.max_up_time, 60L);
	write_sysfs_int(mpu_pedo.min_energy, 0x0c000000L);

	if (mEnabled || mDmpOrientationEnabled || mDmpPedometerEnabled) 
		masterEnable(1);

    return INV_SUCCESS;
}

int MPLSensorSysPed::inv_get_dmp_pedo_walktime(long long *walktime)
{
    VFUNC_LOG;

    long long data = 0;
    int ret;

    /* sysfs for walk time returns unsigned int (mS) */
    ret = read_sysfs_longlong((char*)mpu_pedo.walk_time, &data);
    if (ret == INV_SUCCESS) {
        *walktime = data;
    } else {
        *walktime = 0;
        LOGE("HAL:pedometer - error reading data from %s", mpu_pedo.walk_time);
    }

    return ret;
}

int MPLSensorSysPed::inv_get_dmp_pedo_steps(int *steps)
{
    VFUNC_LOG;

    int data = 0;
    int ret;
    int i;

    /* sysfs for walk steps returns int (steps) */
    ret = read_sysfs_int(mpu_pedo.steps, &data);
    if (ret == INV_SUCCESS) {
        *steps = data;
    } else {
        *steps = 0;
        LOGE("HAL:pedometer - error reading data from %s", mpu_pedo.steps);
    }

    return ret;
}


int MPLSensorSysPed::inv_init_pedo_sysfs_attributes(void)
{
    VFUNC_LOG;

    unsigned char i = 0;
    char sysfs_path[MAX_SYSFS_NAME_LEN], tbuf[2];
    char *sptr;
    char **dptr;
    int num;

    pedo_sysfs_names_ptr = 
            (char *)malloc(sizeof(char[MAX_SYSFS_ATTRB][MAX_SYSFS_NAME_LEN]));
    sptr = pedo_sysfs_names_ptr;
    if (sptr != NULL) {
        dptr = (char **)&mpu_pedo;
        do {
            *dptr++ = sptr;
            sptr += sizeof(char[MAX_SYSFS_NAME_LEN]);
        } while (++i < MAX_SYSFS_ATTRB);
    } else {
        LOGE("HAL:couldn't alloc mem for sysfs paths");
        return -1;
    }
    
    // get sysfs path 
    inv_get_sysfs_path(sysfs_path);
    if (strcmp(sysfs_path, "") == 0)
        return 0;

    // build MPU pedometer's sysfs paths
    sprintf(mpu_pedo.steps, "%s%s", sysfs_path, "/pedometer_steps");
    sprintf(mpu_pedo.walk_time, "%s%s", sysfs_path, "/pedometer_time");

    sprintf(mpu_pedo.clip_threshold, "%s%s", sysfs_path, "/pedometer_clip_threshold");
    sprintf(mpu_pedo.min_steps, "%s%s", sysfs_path, "/pedometer_min_steps");
    sprintf(mpu_pedo.max_step_buffer_time, "%s%s", sysfs_path, "/pedometer_max_step_buffer_time");
    sprintf(mpu_pedo.threshold, "%s%s", sysfs_path, "/pedometer_threshold");
    sprintf(mpu_pedo.min_up_time, "%s%s", sysfs_path, "/pedometer_min_up_time");
    sprintf(mpu_pedo.max_up_time, "%s%s", sysfs_path, "/pedometer_max_up_time");
    sprintf(mpu_pedo.min_energy, "%s%s", sysfs_path, "/pedometer_min_energy");

#if 0
    // test print sysfs paths
    dptr = (char**)&mpu;
    for (i = 0; i < MAX_SYSFS_ATTRB; i++) {
        LOGE("HAL:sysfs path: %s", *dptr++);
    }
#endif
    return 0;
}

