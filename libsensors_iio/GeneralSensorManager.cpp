/*
 * Copyright (C) 2008 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <fcntl.h>
#include <errno.h>
#include <math.h>
#include <poll.h>
#include <unistd.h>
#include <dirent.h>
#include <sys/select.h>
#include <dlfcn.h>

#include <cutils/log.h>

#include "GeneralSensorManager.h"

/*****************************************************************************/

static struct sensor_t sSensorList[] =
{
    {"Temperature", "ST", 1,
    SENSORS_TEMPERATURE_HANDLE,
    SENSOR_TYPE_AMBIENT_TEMPERATURE, 80.0f, 1.0f, 0.03f, 1000*1000, 0, 0,
	SENSOR_STRING_TYPE_AMBIENT_TEMPERATURE,"",10000,SENSOR_FLAG_CONTINUOUS_MODE, { NULL }},

    {"Humidity", "ST", 1,
    SENSORS_HUMIDITY_HANDLE,
    SENSOR_TYPE_RELATIVE_HUMIDITY, 80.0f, 1.0f, 0.03f, 1000*1000, 0, 0,
	SENSOR_STRING_TYPE_RELATIVE_HUMIDITY,"",10000,SENSOR_FLAG_CONTINUOUS_MODE, { NULL }},

    { "Pressure","digital",1,
    SENSORS_PRESSURE_HANDLE,
    SENSOR_TYPE_PRESSURE, 1100.0f, 0.005f, 0.005f, 0, 0, 0,
	SENSOR_STRING_TYPE_PRESSURE,"",10000,SENSOR_FLAG_CONTINUOUS_MODE, { NULL }},

#ifndef SUPPORT_FOR_IWDS
    { "Heart rate sensor","unknown", 1,
    SENSORS_HEART_HANDLE,
    SENSOR_TYPE_HEART_RATE, 1100.0f, 0.005f, 0.005f, 10000, 0, 0,
	SENSOR_STRING_TYPE_HEART_RATE,"",10000,SENSOR_FLAG_CONTINUOUS_MODE, { NULL }},

#endif
    { "EKG sensor","unknown", 1,
    SENSORS_EKG_HANDLE,
    SENSOR_TYPE_EKG, 1100.0f, 0.005f, 0.005f, 10000, 0, 0,
	SENSOR_STRING_TYPE_EKG,"",10000,SENSOR_FLAG_CONTINUOUS_MODE, { NULL }},

    { "UV sensor","Silicon Labs", 1,
    SENSORS_UV_HANDLE,
    SENSOR_TYPE_UV, 1100.0f, 0.005f, 0.005f, 10000, 0, 0,
	SENSOR_STRING_TYPE_UV,"",10000,SENSOR_FLAG_CONTINUOUS_MODE, { NULL }},

    { "Step Counter","Ingenic", 1,
    SENSORS_STEP_COUNTER_HANDLE,
    SENSOR_TYPE_STEP_COUNTER, 1100.0f, 0.005f, 0.005f, 10000, 0, 0,
	SENSOR_STRING_TYPE_STEP_COUNTER,"",10000,SENSOR_FLAG_CONTINUOUS_MODE, { NULL }},

    { "Proximity","Pixart", 1,
    SENSORS_PROXIMITY_HANDLE,
    SENSOR_TYPE_PROXIMITY, 255.0f, 1.0f, 0.005f, 10000, 0, 0,
    SENSOR_STRING_TYPE_PROXIMITY,"",10000,SENSOR_FLAG_ON_CHANGE_MODE, { NULL }},

    { "Pedometer","freescale", 1,
    SENSORS_PEDOMETER_HANDLE,
    SENSOR_TYPE_STEP_COUNTER, 1100.0f, 0.005f, 0.005f, 10000, 0, 0,
    SENSOR_STRING_TYPE_STEP_COUNTER,"",10000,SENSOR_FLAG_CONTINUOUS_MODE, { NULL }},
};

general_sensor_desc_t GeneralSensorManager::supported_temperature_sensors[] = {
	{ "mvh3000d_temp", "i2c-1/1-0044", {"enable_temperature", "poll_period_ms_temp"}, 
	  EV_ABS, ABS_GAS, SENSORS_TEMPERATURE_HANDLE, SENSOR_TYPE_AMBIENT_TEMPERATURE},
	{ "mvh3004d_temp", "i2c-2/2-0044", {"enable_temperature", "poll_period_ms_temp"},
	  EV_ABS, ABS_GAS, SENSORS_TEMPERATURE_HANDLE, SENSOR_TYPE_AMBIENT_TEMPERATURE},
	{ "hts221_temperature", "i2c-1/1-005f", {"temp_enable_device", "temp_poll_period_ms"},
	  EV_ABS, ABS_GAS, SENSORS_TEMPERATURE_HANDLE, SENSOR_TYPE_AMBIENT_TEMPERATURE},
	{ "lps331ap_temp", "i2c-1/1-005c", {"temp_enable_device", "temp_poll_period_ms"},
	  EV_ABS, ABS_GAS, SENSORS_TEMPERATURE_HANDLE, SENSOR_TYPE_AMBIENT_TEMPERATURE},
	{ "shtc1_temp", "i2c-1/1-0070", {"enable_temperature", "poll_period_ms_temp"}, 
	  EV_ABS, ABS_GAS, SENSORS_TEMPERATURE_HANDLE, SENSOR_TYPE_AMBIENT_TEMPERATURE},
};

general_sensor_desc_t GeneralSensorManager::supported_humidity_sensors[] = {
	{ "mvh3000d_humi","i2c-1/1-0044", {"enable_humidity", "poll_period_ms_humi"},
	  EV_ABS, ABS_MISC, SENSORS_HUMIDITY_HANDLE, SENSOR_TYPE_RELATIVE_HUMIDITY},
	{ "mvh3004d_humi","i2c-2/2-0044", {"enable_humidity", "poll_period_ms_humi"},
	  EV_ABS, ABS_MISC, SENSORS_HUMIDITY_HANDLE, SENSOR_TYPE_RELATIVE_HUMIDITY},
	{ "hts221_humidity","i2c-1/1-005f", {"humi_enable_device", "humi_poll_period_ms"},
	  EV_ABS, ABS_MISC, SENSORS_HUMIDITY_HANDLE, SENSOR_TYPE_RELATIVE_HUMIDITY},
	{ "shtc1_humi","i2c-1/1-0070", {"enable_humidity", "poll_period_ms_humi"},
	  EV_ABS, ABS_MISC, SENSORS_HUMIDITY_HANDLE, SENSOR_TYPE_RELATIVE_HUMIDITY}
};

general_sensor_desc_t GeneralSensorManager::supported_pressure_sensors[] = {
        { "lps331ap_pres", "i2c-1/1-005c", {"pres_enable_device", "pres_poll_period_ms"},
	  EV_ABS, ABS_PRESSURE, SENSORS_PRESSURE_HANDLE, SENSOR_TYPE_PRESSURE},
	{ "bmp280","i2c-1/1-0077", {"enable_device", "poll_period_ms"},
	  EV_MSC, MSC_RAW, SENSORS_PRESSURE_HANDLE, SENSOR_TYPE_PRESSURE},
	{ "pps210a2","i2c-2/2-0044", {"enable", "poll_delay"},
	  EV_MSC, MSC_RAW, SENSORS_PRESSURE_HANDLE, SENSOR_TYPE_PRESSURE},
	{ "bmp280","i2c-2/2-0077", {"enable_device", "poll_period_ms"},
	  EV_MSC, MSC_RAW, SENSORS_PRESSURE_HANDLE, SENSOR_TYPE_PRESSURE}
};

#ifndef SUPPORT_FOR_IWDS
general_sensor_desc_t GeneralSensorManager::supported_heart_rate_sensors[] = {
	{ "ADC_heart_rate","hwmon/hwmon0/device", {"enable_heart", "poll_period_ms_heart"},
	  EV_ABS, NULL_VALUE, SENSORS_HEART_HANDLE, SENSOR_TYPE_HEART_RATE},
	{ "ADC_heart_rate","hwmon/hwmon1/device", {"enable_heart", "poll_period_ms_heart"},
	  EV_ABS, NULL_VALUE, SENSORS_HEART_HANDLE, SENSOR_TYPE_HEART_RATE}
};
#endif
general_sensor_desc_t GeneralSensorManager::supported_ekg_sensors[] = {
	{ "evg_power","platform/evg_power", {"enable_heart_aaa", "poll_period_ms_heart"},
	  NULL_VALUE, NULL_VALUE, NULL_VALUE, NULL_VALUE}
};

general_sensor_desc_t GeneralSensorManager::supported_uv_sensors[] = {
	{ "uv_sensor","i2c-2/2-0060", {"enable_uv_sensor", "poll_period_ms_uv"},
	  EV_MSC, MSC_RAW, SENSORS_UV_HANDLE, SENSOR_TYPE_UV}
};

general_sensor_desc_t GeneralSensorManager::supported_step_counter_sensors[] = {
     { "Step Counter", "iio:device0", {"enable_step_counter", "poll_period_ms_steps"},
       EV_ABS, NULL_VALUE, SENSORS_STEP_COUNTER_HANDLE, SENSOR_TYPE_STEP_COUNTER}
};

general_sensor_desc_t GeneralSensorManager::supported_proximity_sensors[] = {
     { "pac7673", "pac7673", {"enable_proximity", "poll_period_ms_proximity"},
       EV_ABS, NULL_VALUE, SENSORS_PROXIMITY_HANDLE, SENSOR_TYPE_PROXIMITY}
};

general_sensor_desc_t GeneralSensorManager::supported_pedometer_sensors[] = {
     { "FreescaleAccelerometer", "mma955xl", {"enable_pedometer", "poll_interval_pedometer"},
       EV_ABS, NULL_VALUE, SENSORS_PEDOMETER_HANDLE, SENSOR_TYPE_STEP_COUNTER}
};

general_sensor_desc_t* GeneralSensorManager::general_sensor_scan_map[GENERAL_SENSOR_NUM] = {
	supported_temperature_sensors,
	supported_humidity_sensors,
	supported_pressure_sensors,
#ifndef SUPPORT_FOR_IWDS
	supported_heart_rate_sensors,
#endif
	supported_ekg_sensors,
	supported_uv_sensors,
	supported_step_counter_sensors,
	supported_proximity_sensors,
	supported_pedometer_sensors,
};

const char* GeneralSensorManager::basePaths[GENERAL_SENSOR_NUM] = {
	"/sys/class/i2c-adapter", /* temperature */
	"/sys/class/i2c-adapter", /* humidity */
	"/sys/class/i2c-adapter", /* pressure */
#ifndef SUPPORT_FOR_IWDS
	"/sys/class",             /* heart_rate */
#endif
	"/sys/devices",           /* EKG */
	"/sys/class/i2c-adapter", /* UV */
	"/sys/bus/iio/devices",   /* Step Counter */
	"/sys/kernel",            /* Proximity */
	"/sys/kernel",            /* MMA955XL Step Counter */
};

int GeneralSensorManager::sensorCount[GENERAL_SENSOR_NUM] = {
	5, /*supported temperature sensor count*/
	4, /*supported humidity sensor count*/
	4, /*supported pressure sensor count*/
#ifndef SUPPORT_FOR_IWDS
	2, /*supported heart rate sensor count*/
#endif
	1, /*supported EKG sensor count*/
	1, /*supported UV sensor count*/
	1,  /*supported Step Counter sensor count*/
	1,  /*supported Proximity sensor count*/
	1,  /*supported Pedometer sensor count*/
};

GeneralSensorManager::GeneralSensorManager(struct sensor_t *list, int *num)
{
    LOGI("%s: start to scan general sensors...", __FUNCTION__);
    if (list != NULL && num != NULL)
        scanGeneralSensors(list, num);
}

GeneralSensorManager::~GeneralSensorManager()
{
	LOGI("GeneralSensorManager end");
	for(int i = 0; i < GENERAL_SENSOR_NUM; i++) {
		if (mGeneralSensors[i] != NULL)
			delete mGeneralSensors[i];
	}
}

int GeneralSensorManager::registerGeneralSensors(struct pollfd* fds, int* idtofd, int* startfd)
{
	LOGI("%s", __FUNCTION__);
	int id = *startfd;
	for(int i = 0; i < GENERAL_SENSOR_NUM; i++) {
		if (mGeneralSensors[i] == NULL) continue;
		(fds + *startfd)->fd = mGeneralSensors[i]->getFd();
		(fds + *startfd)->events = POLLIN;
		(fds + *startfd)->revents = 0;
		*(idtofd + *startfd) = id + i;

		(*startfd)++;
	}
	return *startfd - id;
}

const char* GeneralSensorManager::getTAG(int type)
{
    switch (type) {
    case TEMPERATURE_SENSOR:
	    return "Temperature";
    case HUMIDITY_SENSOR:
	    return "Humidity";
    case PRESSURE_SENSOR:
	    return "Pressure";
#ifndef SUPPORT_FOR_IWDS
    case HEART_RATE_SENSOR:
	    return "Heart_Rate";
#endif
    case EKG_SENSOR:
	    return "EKG";
    case UV_SENSOR:
	    return "UV";
    case STEP_COUNTER_SENSOR:
        return "Step Counter";
    case PROXIMITY_SENSOR:
        return "Proximity";
    case PEDOMETER_SENSOR:
        return "Pedometer";
    }
    return "GeneralSensor";
}

void GeneralSensorManager::scanGeneralSensors(struct sensor_t *list, int *num)
{
    int count = 0;

    for (int i = 0; i < GENERAL_SENSOR_NUM; i++) {
        LOGW("%s sensor:", getTAG(i));
        mGeneralSensors[i] = scanGeneralDevice(basePaths[i],
                general_sensor_scan_map[i], sensorCount[i]);

        if (mGeneralSensors[i] == NULL) {
            LOGW("No %s sensor!", getTAG(i));
            continue;
        } else {
            memcpy(&list[count++], &sSensorList[i], sizeof(struct sensor_t));
            (*num) ++;
        }

        //EKG hasn't input event, only control it's power.
        if (i == EKG_SENSOR)
            mGeneralSensors[i]->setDevicePath(basePaths[i],
                    mGeneralSensors[i]->getSensorDesc()->sys_dev_path);
    }

}

GeneralSensor* GeneralSensorManager::scanGeneralDevice(const char* basePath, general_sensor_desc_t *supported, int length)
{
	int supportedId;
	int adapter = 0;
	char dirname[PATH_MAX];
	char devname[PATH_MAX];
	char *filename;
	DIR *dir;
	struct dirent *de;

        for (supportedId = 0; supportedId < length; supportedId++) {
		adapter = 0;
		sprintf(dirname, "%s/%s/", basePath, supported[supportedId].sys_dev_path);
		LOGI("%s", dirname);
		dir = opendir(dirname);

		if(dir == NULL) continue;

		strcpy(devname, dirname);
		filename = devname + strlen(devname);
		*filename++ = '/';

		while((de = readdir(dir))) {
			if(de->d_name[0] == '.' &&
			   (de->d_name[1] == '\0' || (de->d_name[1] == '.' && de->d_name[2] == '\0'))) {
				continue;
			}

			for(int j = 0; j < generalEventCount; j++) {
				if (!strcmp(de->d_name, supported[supportedId].sensor_events.enable) ||
				    !strcmp(de->d_name, supported[supportedId].sensor_events.delay)) {
					adapter++;
				}
			}
		}
		closedir(dir);

		if (adapter >= generalEventCount) {
			LOGI("%s is selected.", supported[supportedId].sensor_name);
			return new GeneralSensor(basePath, supported + supportedId);
		}
	}
	return NULL;
}

int GeneralSensorManager::enable(int handle, int enabled)
{
	//for no physical sensor, because upper layer don't know that.
	if (mGeneralSensors[handle - GENERAL_SENSORS_START_HANDLE] == NULL) return 0;
	return mGeneralSensors[handle - GENERAL_SENSORS_START_HANDLE]->enable(enabled);
}

int GeneralSensorManager::setDelay(int handle, int64_t ns)
{
	if (mGeneralSensors[handle - GENERAL_SENSORS_START_HANDLE] == NULL) return 0;
	return mGeneralSensors[handle - GENERAL_SENSORS_START_HANDLE]->setDelay(ns);
}

int GeneralSensorManager::readGeneralSensorEvents(sensors_event_t *data, int count, int type)
{
	int total = 0;

	if (count <= 0)	return 0;

	data->version = sizeof(sensors_event_t);

	input_event const* event;

	if (mGeneralSensors[type] != NULL) {
		const char* name = mGeneralSensors[type]->getName();
		//ignore EVG, never get data from here.
		if (!strcmp(name, "evg_power")) return 0;
		int cnt = mGeneralSensors[type]->readEvents(data, count);

		total += cnt;
		count -= cnt;
			
		/* convert algorithm */
		//temperature
		if (!strcmp(name, "mvh3000d_temp")){
			data->temperature = (float)data->data[0] / 100.0f;
			
		} else if (!strcmp(name, "mvh3004d_temp")){
			data->temperature = (float)data->data[0] / 100.0f;

		} else if (!strcmp(name,"hts221_temperature")){
			data->temperature = (float)data->data[0];
			
		} else if (!strcmp(name,"lps331ap_temp")){
			data->temperature = 42.5f + (float)data->data[0] / 480.0f;

		} else if (!strcmp(name,"shtc1_temp")){
			data->temperature = 175 * (float)data->data[0] / 65536 - 45;
		}
		
		//humidity
		else if (!strcmp(name,"mvh3000d_humi")){
			float tmp =  (float)data->data[0] / 100.0f;
			if (tmp > 0 && tmp <= 40)
				tmp += 7;
			else if (tmp > 40)
				tmp += 10;
			data->relative_humidity = tmp;
			
		} else if (!strcmp(name,"mvh3004d_humi")){
			float tmp =  (float)data->data[0] / 100.0f;
			if (tmp > 0 && tmp <= 40)
				tmp += 7;
			else if (tmp > 40)
				tmp += 10;
			data->relative_humidity = tmp;

		} else if (!strcmp(name,"hts221_humidity")){
			data->relative_humidity = (float)data->data[0];

		} else if (!strcmp(name,"shtc1_humi")){
			data->relative_humidity = 100 * (float)data->data[0] / 65536;
		}

		//pressure
		else if (!strcmp(name,"bmp280")){
			data->pressure = (float)data->data[0] / 100.0f;
		} else if (!strcmp(name,"pps210a2")){
			data->pressure = (float)data->data[0] / 100.0f;
		} else if (!strcmp(name,"lps331ap_pres")){
			data->pressure = (float)data->data[0] / 4096.0f;
		}
#ifndef SUPPORT_FOR_IWDS
		//heart rate
		else if (!strcmp(name,"ADC_heart_rate")){
			data->relative_heart = (float)data->data[0];
		}
#endif
		//uv
		else if (!strcmp(name,"uv_sensor")){
			data->uv_index = (float)data->data[0];
		}
		
        else if (!strcmp(name,"Step Counter")){
            //do nothing
        }

        else if (!strcmp(name,"Proximity")){
            //do nothing
        }

		// mma955xl step count
        else if (!strcmp(name,"Pedometer")){
           //do nothing
        }


	}
	
	return total;
}
