#ifndef HEART_RATE_SENSOR_ADC_H
#define HEART_RATE_SENSOR_ADC_H

#include <stdint.h>
#include <errno.h>
#include <sys/cdefs.h>
#include <sys/types.h>
#include <pthread.h>

#include "sensors.h"
#include "SensorBase.h"
#include "GeneralSensor.h"
#include "InputEventReader.h"
#include "sensor_params.h"
#include "algorithm/AlgHeartRateAdc.h"

#define NULL_VALUE (-100)
#define NS_TO_MS(n) (n / (1000 * 1000))

typedef struct {
    const char *enable;
    const char *delay;
} heart_rate_ctrl_desc_t;

typedef struct {
    const char *name;
    const char *sysPath;
    const heart_rate_ctrl_desc_t ctrlDesc;
    const int32_t eventType;
    const int32_t eventCode;
    const int32_t handle;
    const int32_t type;
} heart_rate_desc_t;

class HeartRateSensorAdc : SensorBase {
public:
    static heart_rate_desc_t heart_rate_sensor_map[];

    HeartRateSensorAdc();
    virtual ~HeartRateSensorAdc();

    int enable(int enabled);
    int setDelay(int64_t ns);
    int readInput(sensors_event_t *event, int count);
    int readEvents(sensors_event_t *event, int count);
    int getFd() const;
    bool isExist() const;
    int populateSensorList(struct sensor_t *list);

private:
    bool scanSensor();
    bool intialize();
    static void *threadLoop(void *param);

    int mMainWriteFd;
    int mMainReadFd;
    int mWorkerWriteFd;
    int mWorkerReadFd;
    bool mEnabled;
    bool mExist;
    int mHeartRate;
    bool mStartThread;
    int64_t mDelay;
    int64_t mTimestamp;
    InputEventCircularReader mGeneralInputReader;
    AlgHeartRateAdc* mAlgHeartRateAdc;

    heart_rate_desc_t *mDesc;
    char input_sysfs_path[PATH_MAX];
    int input_sysfs_path_len;
};

#endif //HEART_RATE_SENSOR_ADC_H
