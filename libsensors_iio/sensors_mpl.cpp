/*
* Copyright (C) 2012 Invensense, Inc.
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*      http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/

#define FUNC_LOG LOGV("%s", __PRETTY_FUNCTION__)

#include <hardware/sensors.h>
#include <fcntl.h>
#include <errno.h>
#include <dirent.h>
#include <math.h>
#include <poll.h>
#include <pthread.h>
#include <stdlib.h>
#include <string.h>

#include <linux/input.h>

#include <utils/Atomic.h>
#include <utils/Log.h>

#include "sensors.h"
#include "MPLSensor.h"
#include "MPLSensorSysPed.h"
#include "GeneralSensorManager.h"
#include "FrizzSensor.h"
#include "HeartRateSensorAdc.h"
#include "HeartRateSensorPixArt.h"
#include "HeartRateSensorIngenic.h"
#include "VoiceTriggerSensor.h"

/* The SENSORS Module */
/**
 * ADC heart rate sensor
 * PixArt heart rate sensor
 * Voice trigger sensor
 */
#define MISC_SENSOR_NUM   3

#if defined(ENABLE_DMP_SCREEN_AUTO_ROTATION) && defined(ENABLE_DMP_TAP)
#define LOCAL_SENSORS (GeneralSensorManager::GENERAL_SENSOR_NUM + MPLSensor::NumSensors + MISC_SENSOR_NUM + 2)
#elif defined(ENABLE_DMP_SCREEN_AUTO_ROTATION) || defined(ENABLE_DMP_TAP)
#define LOCAL_SENSORS (GeneralSensorManager::GENERAL_SENSOR_NUM + MPLSensor::NumSensors + MISC_SENSOR_NUM+ 1)
#else
#define LOCAL_SENSORS (GeneralSensorManager::GENERAL_SENSOR_NUM + MPLSensor::NumSensors + MISC_SENSOR_NUM)
#endif

/* Vendor-defined Accel Load Calibration File Method
* @param[out] Accel bias, length 3.  In HW units scaled by 2^16 in body frame
* @return '0' for a successful load, '1' otherwise
* example: int AccelLoadConfig(long* offset);
* End of Vendor-defined Accel Load Cal Method
*/

static char *gId;

static struct sensor_t sSensorListForAndroid[LOCAL_SENSORS];
static int sensorsForAndroid = (sizeof(sSensorListForAndroid) / sizeof(sensor_t));

static struct sensor_t sSensorListForIwds[LOCAL_SENSORS];
static int sensorsForIwds = (sizeof(sSensorListForIwds) / sizeof(sensor_t));

static int open_sensors(const struct hw_module_t* module, const char* id,
                        struct hw_device_t** device);

static int sensors_get_sensors_list_for_iwds(struct sensors_module_t* module,
                                     struct sensor_t const** list)
{
    *list = sSensorListForIwds;
    return sensorsForIwds;
}

static struct hw_module_methods_t sensors_module_methods_for_iwds = {
        open: open_sensors
};

static int sensors_get_sensors_list_for_android(struct sensors_module_t* module,
                                     struct sensor_t const** list)
{
    *list = sSensorListForAndroid;
    return sensorsForAndroid;
}

static struct hw_module_methods_t sensors_module_methods_for_android = {
        open: open_sensors
};

struct sensors_module_t HAL_MODULE_INFO_SYM[2] = {
        {
            common: {
                    tag: HARDWARE_MODULE_TAG,
                    version_major: 1,
                    version_minor: 0,
                    id: SENSORS_HARDWARE_MODULE_ID,
                    name: SMART_SENSE_MODULE_NAME,
                    author: "Ingenic",
                    methods: &sensors_module_methods_for_android,
            },
            get_sensors_list: sensors_get_sensors_list_for_android,
        },

        {
            common: {
                    tag: HARDWARE_MODULE_TAG,
                    version_major: 1,
                    version_minor: 0,
                    id: SENSORS_HARDWARE_MODULE_ID,
                    name: SMART_SENSE_MODULE_NAME,
                    author: "Ingenic",
                    methods: &sensors_module_methods_for_iwds,
            },
            get_sensors_list: sensors_get_sensors_list_for_iwds,
        },
};

struct sensors_poll_context_t {
    struct sensors_poll_device_t device; // must be first

    sensors_poll_context_t();
    ~sensors_poll_context_t();
    int activate(int handle, int enabled);
    int setDelay(int handle, int64_t ns);
    int pollEvents(sensors_event_t* data, int count);
#ifndef INVENSENSE_COMPASS_CAL
    int calibrate(int handle, int *data);
#endif
    int setRightHand(int handle, int isRightHand);

private:
    enum {
        mpl = 0,
        compass,
        dmpOrient,
        dmpTap,
        frizz,
        ingenic_heart_rate,
#ifdef SUPPORT_FOR_IWDS
        adc_heart_rate,
        pixart_heart_rate,
        voice_trigger,
#endif
	temperature,
	humidity,
	pressure,
#ifndef SUPPORT_FOR_IWDS
	heart_rate,
#endif
	ekg,
	uv,
	step_counter,
	proximity,
	pedometer,
        numSensorDrivers,   // wake pipe goes here
        numFds
    };

    const char* typeToString(int type);
    void dumpSensorList() const;

    struct pollfd mPollFds[numSensorDrivers];
    int idToFd[numSensorDrivers];
    int sensorId = 0;
    SensorBase *mSensor;
    CompassSensor *mCompassSensor;

    GeneralSensorManager *mGeneralSensorManager;
    FrizzSensor *mFrizzSensor;
    HeartRateSensorIngenic *mHeartRateSensorIngenic;
    HeartRateSensorAdc* mHeartRateSensorAdc;
    HeartRateSensorPixArt* mHeartRateSensorPixArt;
    VoiceTriggerSensor *mVoiceTriggerSensor;

#ifndef INVENSENSE_COMPASS_CAL
    SensorBase* mSensorsNew[numSensorDrivers];
    int handleToDriver(int handle) const {
	    switch (handle) {
		    case ID_M:
			    return compass;
		    default:
			    return -EINVAL;
	    }
	    return -EINVAL;
    }
#endif
};

const char* sensors_poll_context_t::typeToString(int type)
{
    switch(type) {
    case mpl:
        return "mpl";
    case compass:
        return "compass";
    case dmpOrient:
        return "dmpOrient";
    case dmpTap:
        return "dmpTap";
    case frizz:
        return "frizz";
    case ingenic_heart_rate:
        return "ingenic_heart_rate";
#ifdef SUPPORT_FOR_IWDS
    case adc_heart_rate:
        return "adc_heart_rate";
    case pixart_heart_rate:
        return "pixart_heart_rate";
    case voice_trigger:
        return "voice_trigger";
#endif
    case temperature:
        return "temperature";
    case humidity:
        return "humidity";
    case pressure:
        return "pressure";
#ifndef SUPPORT_FOR_IWDS
    case heart_rate:
        return "heart_rate";
#endif
    case ekg:
        return "ekg";
    case uv:
        return "uv";
    case step_counter:
        return "step_counter";
    case proximity:
        return "proximity";
    case pedometer:
        return "pedometer";
    default:
        return "unknown";
    }
}

void sensors_poll_context_t::dumpSensorList() const
{
    if (!strcmp(gId, SENSORS_HARDWARE_POLL)) {
        LOGI("==================================================");
        LOGI("Dump Android Sensor List");
        int i = 0;
        for (; i < sensorsForAndroid; i++) {
            LOGI("%s", sSensorListForAndroid[i].name);
        }
        LOGI("==================================================");
    }

    if (!strcmp(gId, SENSORS_HARDWARE_POLL_FOR_IWDS)) {
        LOGI("==================================================");
        LOGI("Dump IWDS Sensor List");
        int i = 0;
        for (; i < sensorsForIwds; i++) {
            LOGI("%s", sSensorListForIwds[i].name);
        }
        LOGI("==================================================");
    }
}

sensors_poll_context_t::sensors_poll_context_t()
{
    VFUNC_LOG;

    MPLSensor *mplSensor = NULL;

    sensorsForAndroid = 0;
    sensorsForIwds = 0;

    if (!strcmp(gId, SENSORS_HARDWARE_POLL)) {
        mCompassSensor = new CompassSensor();
        mplSensor = new MPLSensorSysPed(mCompassSensor);
        if (!mplSensor->isExit()) {
            delete mplSensor;
            delete mCompassSensor;
            mplSensor = NULL;
            mCompassSensor = NULL;
            LOGW("MPLSensor not exist");
        } else {
            // setup the callback object for handing mpl callbacks
            setCallbackObject(mplSensor);

            // populate the sensor list
            sensorsForAndroid = mplSensor->populateSensorList(sSensorListForAndroid,
                    sizeof(sSensorListForAndroid));
        }
    }

#ifdef SUPPORT_FOR_IWDS
    /*
     * Note:
     *     general sensors move to iwds sensors list
     */
    if (!strcmp(gId, SENSORS_HARDWARE_POLL_FOR_IWDS)) {
        mGeneralSensorManager = new GeneralSensorManager(&sSensorListForIwds[sensorsForIwds],
                &sensorsForIwds);
    }
#else
    if (!strcmp(gId, SENSORS_HARDWARE_POLL)) {
        mGeneralSensorManager = new GeneralSensorManager(&sSensorListForAndroid[sensorsForAndroid],
                &sensorsForAndroid);
    }
#endif

#ifdef SUPPORT_FOR_IWDS
    if (!strcmp(gId, SENSORS_HARDWARE_POLL_FOR_IWDS)) {
        mHeartRateSensorAdc = new HeartRateSensorAdc();
        if (!mHeartRateSensorAdc->isExist()) {
            delete mHeartRateSensorAdc;
            mHeartRateSensorAdc = NULL;
        } else {
            sensorsForIwds += mHeartRateSensorAdc->populateSensorList(&sSensorListForIwds[sensorsForIwds]);
        }
    } else {
        mHeartRateSensorAdc = NULL;
    }

    if (!strcmp(gId, SENSORS_HARDWARE_POLL_FOR_IWDS)) {
        mHeartRateSensorPixArt = new HeartRateSensorPixArt();
        if (!mHeartRateSensorPixArt->isExist()) {
            delete mHeartRateSensorPixArt;
            mHeartRateSensorPixArt = NULL;
        } else {
            sensorsForIwds += mHeartRateSensorPixArt->populateSensorList(&sSensorListForIwds[sensorsForIwds]);
        }
    } else {
        mHeartRateSensorPixArt = NULL;
    }

    if (!strcmp(gId, SENSORS_HARDWARE_POLL_FOR_IWDS)) {
        mVoiceTriggerSensor = new VoiceTriggerSensor();
        if (!mVoiceTriggerSensor->isExist()) {
            delete mVoiceTriggerSensor;
            mVoiceTriggerSensor = NULL;
        } else {
            sensorsForIwds += mVoiceTriggerSensor->populateSensorList(&sSensorListForIwds[sensorsForIwds]);
        }
    } else {
        mVoiceTriggerSensor = NULL;
    }
#endif

    /*
     * Note:
     *     sensors connected to Frizz be added to different sensors list
     *     according their type
     */
    struct sensor_t *ps = NULL;
    int *nList = NULL;
    if (!strcmp(gId, SENSORS_HARDWARE_POLL)) {
        mFrizzSensor = new FrizzSensor(FrizzSensor::FRIZZ_GENERAL_HAL);
        ps = &sSensorListForAndroid[sensorsForAndroid];
        nList = &sensorsForAndroid;
    } else if (!strcmp(gId, SENSORS_HARDWARE_POLL_FOR_IWDS)) {
        mFrizzSensor = new FrizzSensor(FrizzSensor::FRIZZ_SPECIAL_HAL);
        ps = &sSensorListForIwds[sensorsForIwds];
        nList = &sensorsForIwds;
    }

    if (!mFrizzSensor->isEnabled()) {
        delete mFrizzSensor;
        mFrizzSensor = NULL;
        LOGW("Frizz not exist");
    } else {
        *nList += mFrizzSensor->populateSensorList(ps);
    }

    if (!strcmp(gId, SENSORS_HARDWARE_POLL_FOR_IWDS)) {
        mHeartRateSensorIngenic = new HeartRateSensorIngenic();
        if (!mHeartRateSensorIngenic->isExist()) {
            delete mHeartRateSensorIngenic;
            mHeartRateSensorIngenic = NULL;
        } else {
            sensorsForIwds += mHeartRateSensorIngenic->populateSensorList(&sSensorListForIwds[sensorsForIwds]);
        }
    } else {
        mHeartRateSensorIngenic = NULL;
    }

    if (!strcmp(gId, SENSORS_HARDWARE_POLL))
        LOGI("populate Android Sensor List Number %d", sensorsForAndroid);

    if (!strcmp(gId, SENSORS_HARDWARE_POLL_FOR_IWDS))
        LOGI("populate Iwds Sensor List Number %d", sensorsForIwds);

   /* For Vendor-defined Accel Calibration File Load
    * Use the Following Constructor and Pass Your Load Cal File Function
    *
	* MPLSensor *mplSensor = new MPLSensor(mCompassSensor, AccelLoadConfig);
	*/

    idToFd[sensorId++] = mpl;
    idToFd[sensorId++] = compass;
    idToFd[sensorId++] = dmpOrient;
    idToFd[sensorId++] = dmpTap;
    if (mCompassSensor != NULL && mplSensor != NULL) {
        if (!strcmp(gId, SENSORS_HARDWARE_POLL)) {
            mSensor = mplSensor;
            mPollFds[mpl].fd = mSensor->getFd();
            mPollFds[mpl].events = POLLIN;
            mPollFds[mpl].revents = 0;

            mPollFds[compass].fd = mCompassSensor->getFd();
            mPollFds[compass].events = POLLIN;
            mPollFds[compass].revents = 0;

            mPollFds[dmpOrient].fd = ((MPLSensor*) mSensor)->getDmpOrientFd();
            mPollFds[dmpOrient].events = POLLPRI;
            mPollFds[dmpOrient].revents = 0;

            mPollFds[dmpTap].fd = ((MPLSensor*) mSensor)->getDmpTapFd();
            mPollFds[dmpTap].events = POLLPRI;
            mPollFds[dmpTap].revents = 0;
        }
    } else {
        mPollFds[mpl].fd = -1;
        mPollFds[compass].fd = -1;
        mPollFds[dmpOrient].fd = -1;
        mPollFds[dmpTap].fd = -1;
    }

    idToFd[sensorId++] = frizz;
    if (mFrizzSensor != NULL) {
        mPollFds[frizz].fd = mFrizzSensor->getFd();
        mPollFds[frizz].events = POLLIN;
        mPollFds[frizz].revents = 0;

        ALOGE("mFrizzSensor getfd %d", mPollFds[frizz].fd);
    } else {
        mPollFds[frizz].fd = -1;
    }
    idToFd[sensorId++] = ingenic_heart_rate;
    if (mHeartRateSensorIngenic != NULL) {
        mPollFds[ingenic_heart_rate].fd = mHeartRateSensorIngenic->getFd();
        mPollFds[ingenic_heart_rate].events = POLLIN;
        mPollFds[ingenic_heart_rate].revents = 0;
        ALOGE("mHeartRateSensorIngenic getfd %d", mPollFds[ingenic_heart_rate].fd);
    } else {
        mPollFds[ingenic_heart_rate].fd = -1;
    }

#ifdef SUPPORT_FOR_IWDS
    idToFd[sensorId++] = adc_heart_rate;
    if (mHeartRateSensorAdc != NULL) {
        mPollFds[adc_heart_rate].fd = mHeartRateSensorAdc->getFd();
        mPollFds[adc_heart_rate].events = POLLIN;
        mPollFds[adc_heart_rate].revents = 0;
    } else {
        mPollFds[adc_heart_rate].fd = -1;
    }

    idToFd[sensorId++] = pixart_heart_rate;
    if (mHeartRateSensorPixArt != NULL) {
        mPollFds[pixart_heart_rate].fd = mHeartRateSensorPixArt->getFd();
        mPollFds[pixart_heart_rate].events = POLLIN;
        mPollFds[pixart_heart_rate].revents = 0;
    } else {
        mPollFds[pixart_heart_rate].fd = -1;
    }

    idToFd[sensorId++] = voice_trigger;
    if (mVoiceTriggerSensor != NULL) {
        mPollFds[voice_trigger].fd = mVoiceTriggerSensor->getFd();
        mPollFds[voice_trigger].events = POLLIN;
        mPollFds[voice_trigger].revents = 0;
    } else {
        mPollFds[voice_trigger].fd = -1;
    }
#endif

#ifdef SUPPORT_FOR_IWDS
    if (!strcmp(gId, SENSORS_HARDWARE_POLL_FOR_IWDS)) {
        int gc = mGeneralSensorManager->registerGeneralSensors(mPollFds, idToFd, &sensorId);
        LOGI("%s general sensor count:%d", __FUNCTION__, gc);
    }
#else
    if (!strcmp(gId, SENSORS_HARDWARE_POLL)) {
        int gc = mGeneralSensorManager->registerGeneralSensors(mPollFds, idToFd, &sensorId);
        LOGI("%s general sensor count:%d", __FUNCTION__, gc);
    }
#endif

#ifdef DEBUG
    dumpSensorList();
#endif
}

sensors_poll_context_t::~sensors_poll_context_t() {
	FUNC_LOG;
    if (!strcmp(gId, SENSORS_HARDWARE_POLL)) {
        if (mSensor)
            delete mSensor;
        if (mCompassSensor)
            delete mCompassSensor;
    }

#ifdef SUPPORT_FOR_IWDS
    if (!strcmp(gId, SENSORS_HARDWARE_POLL_FOR_IWDS)) {
        if (mGeneralSensorManager != NULL)
            delete mGeneralSensorManager;
    }
#else
    if (!strcmp(gId, SENSORS_HARDWARE_POLL)) {
        if (mGeneralSensorManager != NULL)
            delete mGeneralSensorManager;
    }
#endif

    if (mFrizzSensor != NULL)
        delete mFrizzSensor;

    if (mHeartRateSensorIngenic != NULL)
        delete mHeartRateSensorIngenic;
    if (mHeartRateSensorAdc != NULL)
        delete mHeartRateSensorAdc;

    if (mHeartRateSensorPixArt != NULL)
        delete mHeartRateSensorPixArt;

    if (mVoiceTriggerSensor != NULL)
        delete mVoiceTriggerSensor;

    if (gId != NULL)
        free(gId);
}

int sensors_poll_context_t::activate(int handle, int enabled)
{
    FUNC_LOG;

    if (handle == SENSORS_ADC_HEART_RATE_HANDLE) {
        return mHeartRateSensorAdc->enable(enabled);
    }

    if (handle == SENSORS_PIXART_HEART_RATE_HANDLE) {
        return mHeartRateSensorPixArt->enable(enabled);
    }

    if (handle == SENSORS_VOICE_TRIGGER_HANDLE) {
        return mVoiceTriggerSensor->enable(enabled);
    }

    if (handle >= SENSORS_ID_FRIZZ_BEGIN && handle <= SENSORS_ID_FRIZZ_END) {
        return mFrizzSensor->activate(handle, enabled);
    }

    if (handle == SENSORS_INGENIC_HEART_RATE_HANDLE) {
        return mHeartRateSensorIngenic->enable(handle, enabled);
    }

    if (handle >= GENERAL_SENSORS_START_HANDLE
            && handle <= GENERAL_SENSORS_END_HANDLE) {

        return mGeneralSensorManager->enable(handle, enabled);
    }
    return mSensor->enable(handle, enabled);
}

#ifndef INVENSENSE_COMPASS_CAL
int sensors_poll_context_t::calibrate(int handle, int *data) {
    return mSensor->calibrate(handle, data);
}
#endif

int sensors_poll_context_t::setDelay(int handle, int64_t ns)
{
    FUNC_LOG;

    if (handle == SENSORS_ADC_HEART_RATE_HANDLE) {
        return mHeartRateSensorAdc->setDelay(ns);
    }

    if (handle == SENSORS_PIXART_HEART_RATE_HANDLE) {
        return mHeartRateSensorPixArt->setDelay(ns);
    }

    if (handle == SENSORS_VOICE_TRIGGER_HANDLE) {
        return mVoiceTriggerSensor->setDelay(ns);
    }

    if (handle >= SENSORS_ID_FRIZZ_BEGIN && handle <= SENSORS_ID_FRIZZ_END){
        return mFrizzSensor->setDelay(handle, ns);
    }

    if (handle == SENSORS_INGENIC_HEART_RATE_HANDLE) {
        return mHeartRateSensorIngenic->setDelay(handle, ns);
    }

    if (handle >= GENERAL_SENSORS_START_HANDLE && handle <= GENERAL_SENSORS_END_HANDLE){
	    return mGeneralSensorManager->setDelay(handle, ns);
    }
    return mSensor->setDelay(handle, ns);
}

int sensors_poll_context_t::pollEvents(sensors_event_t *data, int count)
{
    VHANDLER_LOG;
    int nbEvents = 0;
    int nb, polltime = -1;

    // look for new events
    nb = poll(mPollFds, sensorId, polltime);

    if (nb > 0) {
        for (int i = 0; count && i < sensorId; i++) {
	    int fd = idToFd[i];
#ifdef DEBUG
	    //LOGI("=====> %s-%d has event pending <=====", typeToString(fd), fd);
#endif
            if (mPollFds[i].revents & (POLLIN | POLLPRI)) {
                nb = 0;
                if (fd == mpl) {
                    ((MPLSensor*) mSensor)->buildMpuEvent();
                    mPollFds[i].revents = 0;
                } else if (fd == compass) {
                    ((MPLSensor*) mSensor)->buildCompassEvent();
                    mPollFds[i].revents = 0;
#ifndef INVENSENSE_COMPASS_CAL
                    nb = ((MPLSensor*) mSensor)->readEvents(data, count);
                    if (nb > 0) {
                        count -= nb;
                        nbEvents += nb;
                        data += nb;
                    }
#endif
                } else if (fd == dmpOrient) {
                    nb = ((MPLSensor*) mSensor)->readDmpOrientEvents(data,
                            count);
                    mPollFds[i].revents = 0;
                    if (isDmpScreenAutoRotationEnabled() && nb > 0) {
                        count -= nb;
                        nbEvents += nb;
                        data += nb;
                    }
                } else if (fd == dmpTap) {
                    nb = ((MPLSensor*) mSensor)->readDmpTapEvents(data, count);
                    mPollFds[i].revents= 0;
                    if (isDmpTapEnabled() && nb > 0) {
                        count -= nb;
                        nbEvents += nb;
                        data += nb;
                    }
                } else if (fd == frizz) {
                    if (mFrizzSensor != NULL) {
                        mPollFds[i].revents = 0;
                        char buf[8];
                        int ret = read(mPollFds[i].fd, buf, sizeof(buf));
                        if (ret == -1) {
                            ALOGE("poll frizz pipe fd error!%d %c", ret,
                                    buf[0]);
                        } else {
                            nb = mFrizzSensor->readEvents(data, count);
                            if (nb > 0) {
                                count -= nb;
                                nbEvents += nb;
                                data += nb;
                            }
                        }
                    }
                } else if (fd == ingenic_heart_rate) {
                    if (mHeartRateSensorIngenic != NULL) {
                        char buf[10];
                        int ret = 0;
                        mPollFds[i].revents = 0;
                        ret = read(mPollFds[i].fd, buf, sizeof(buf));
                        if (ret < 0) {
                            ALOGE("poll ingenic heart rate pipe fd errror\n");
                        } else {
                            nb = mHeartRateSensorIngenic->readEvents(data, count);
                            if (nb > 0) {
                                count -= nb;
                                nbEvents += nb;
                                data += nb;
                            }
                        }
                    }
                }

#ifdef SUPPORT_FOR_IWDS
                else if (fd == adc_heart_rate) {
                    if (mHeartRateSensorAdc != NULL) {
                        mPollFds[i].revents = 0;
                        char buf[8];
                        int ret = read(mPollFds[i].fd, buf, sizeof(buf));
                        if (ret == -1) {
                            ALOGE("poll adc heart pipe fd error!%d %c", ret,
                                    buf[0]);
                        } else {
                            nb = mHeartRateSensorAdc->readEvents(data, count);
                            if (nb > 0) {
                                count -= nb;
                                nbEvents += nb;
                                data += nb;
                            }
                        }
                    }
                }

                else if (fd == pixart_heart_rate) {
                    if (mHeartRateSensorPixArt != NULL) {
                        mPollFds[i].revents = 0;
                        char buf[8];
                        int ret = read(mPollFds[i].fd, buf, sizeof(buf));
                        if (ret == -1) {
                            ALOGE("poll pixart heart pipe fd error!%d %c", ret,
                                    buf[0]);
                        } else {
                            nb = mHeartRateSensorPixArt->readEvents(data, count);
                            if (nb > 0) {
                                count -= nb;
                                nbEvents += nb;
                                data += nb;
                            }
                        }
                    }
                }

                else if (fd == voice_trigger) {
                    if (mVoiceTriggerSensor != NULL) {
                        mPollFds[i].revents = 0;
                        char buf[8];
                        int ret = read(mPollFds[i].fd, buf, sizeof(buf));
                        if (ret == -1) {
                            ALOGE("poll voice trigger fd error!%d %c", ret,
                                    buf[0]);
                        } else {
                            nb = mVoiceTriggerSensor->readEvents(data, count);
                            if (nb > 0) {
                                count -= nb;
                                nbEvents += nb;
                                data += nb;
                            }
                        }
                    }
                }
#endif
                else if (fd >= temperature && fd < numSensorDrivers) {
                    nb = mGeneralSensorManager->readGeneralSensorEvents(data,
                            count, (fd - temperature)/*general type*/);
                    mPollFds[i].revents = 0;
                    if (nb > 0) {
                        count -= nb;
                        nbEvents += nb;
                        data += nb;
                    }
                }
            }
        }

        if (!strcmp(gId, SENSORS_HARDWARE_POLL)) {
            nb = ((MPLSensor*) mSensor)->readEvents(data, count);
            if (nb > 0) {
                count -= nb;
                nbEvents += nb;
                data += nb;
            }
        }
    }

    return nbEvents;
}

int sensors_poll_context_t::setRightHand(int handle, int isRightHand) {
    return mFrizzSensor->setRightHand(handle, isRightHand);
}

static int poll__close(struct hw_device_t *dev)
{
    FUNC_LOG;
    sensors_poll_context_t *ctx = (sensors_poll_context_t *)dev;
    if (ctx) {
        delete ctx;
    }
    return 0;
}

static int poll__activate(struct sensors_poll_device_t *dev,
                          int handle, int enabled)
{
    sensors_poll_context_t *ctx = (sensors_poll_context_t *)dev;
    return ctx->activate(handle, enabled);
}

static int poll__setDelay(struct sensors_poll_device_t *dev,
                          int handle, int64_t ns)
{
    sensors_poll_context_t *ctx = (sensors_poll_context_t *)dev;
    int s= ctx->setDelay(handle, ns);
    return s;
}

static int poll__poll(struct sensors_poll_device_t *dev,
                      sensors_event_t* data, int count)
{
    sensors_poll_context_t *ctx = (sensors_poll_context_t *)dev;
    return ctx->pollEvents(data, count);
}

#ifndef INVENSENSE_COMPASS_CAL
static int poll__calibrate(struct sensors_poll_device_t *dev,
        int handle, int *data) {
    sensors_poll_context_t *ctx = (sensors_poll_context_t *)dev;
    return ctx->calibrate(handle, data);
}
#endif

static int poll__setRightHand(struct sensors_poll_device_t *dev,
        int handle, int isRightHand) {
    sensors_poll_context_t *ctx = (sensors_poll_context_t *)dev;
    return ctx->setRightHand(handle, isRightHand);
}

/** Open a new instance of a sensor device using name */
static int open_sensors(const struct hw_module_t* module, const char* id,
                        struct hw_device_t** device)
{
    FUNC_LOG;
    int status = -EINVAL;

    gId = strdup(id);

    sensors_poll_context_t *dev = new sensors_poll_context_t();

    memset(&dev->device, 0, sizeof(sensors_poll_device_t));

    dev->device.common.tag = HARDWARE_DEVICE_TAG;
    dev->device.common.version  = 0;
    dev->device.common.module   = const_cast<hw_module_t*>(module);
    dev->device.common.close    = poll__close;
    dev->device.activate        = poll__activate;
    dev->device.setDelay        = poll__setDelay;
    dev->device.poll            = poll__poll;
#ifndef INVENSENSE_COMPASS_CAL
    dev->device.calibrate  = poll__calibrate;
#endif
    dev->device.setRightHand        = poll__setRightHand;

    *device = &dev->device.common;
    status = 0;

    return status;
}
