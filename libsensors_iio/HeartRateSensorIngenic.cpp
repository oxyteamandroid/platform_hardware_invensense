/*
 *  Copyright (C) 2015 Wu Jiao <jiao.wu@ingenic.com wujiaososo@qq.com>
 *
 *  This program is free software; you can redistribute it and/or modify it
 *  under  the terms of the GNU General  Public License as published by the
 *  Free Software Foundation;  either version 2 of the License, or (at your
 *  option) any later version.
 *
 *  You should have received a copy of the GNU General Public License along
 *  with this program; if not, write to the Free Software Foundation, Inc.,
 *  675 Mass Ave, Cambridge, MA 02139, USA.
 */

#include <fcntl.h>
#include <errno.h>
#include <math.h>
#include <poll.h>
#include <unistd.h>
#include <dirent.h>
#include <sys/select.h>
#include <dlfcn.h>
#include <cutils/log.h>
#include <hardware/sensors.h>
#include "HeartRateSensorIngenic.h"
#include "sensors.h"
#include "sensor_params.h"

static sensor_t sSensor = {
		"Ingenic Heart rate sensor",
		"Ingenic",
		1,
		SENSORS_INGENIC_HEART_RATE_HANDLE,
		SENSOR_TYPE_HEART_RATE,
		500.0f,
		1.0f,
		0.005f,
		10000,
		0, 0,
		SENSOR_STRING_TYPE_HEART_RATE,
		"", 10000,
		SENSOR_FLAG_CONTINUOUS_MODE,
		{}
};

HeartRateSensorIngenic::HeartRateSensorIngenic(void): SensorBase(NULL, NULL) {
	int ret;

	ret = initialize();
	if (ret) {
		pr_err("ingenic heart rate failed to initialize\n");
		mInited = 0;
		mEnabled = 0;
	} else {
		mInited = 1;
		mEnabled = 0;
	}
}

HeartRateSensorIngenic::~HeartRateSensorIngenic(void) {
	enable(SENSORS_INGENIC_HEART_RATE_HANDLE, 0);
	if (pollfd)
		close(pollfd);
	if (wakefd)
		close(wakefd);

	release_heart_rate_dev_module();
}

int HeartRateSensorIngenic::initialize(void) {
	int mpipe[2];
	int ret;
	int errs = 0;

	ret = init_heart_rate_dev_module();
	if (ret) {
		pr_err("failed to init heart reate dev module\n");
		errs += 1;
	}

	ret = pipe(mpipe);
	if (ret < 0) {
		pr_err("failed to create pipe: (%s)\n", strerror(errno));
		errs += 1;
	}

	pollfd = mpipe[0];
	wakefd = mpipe[1];

	if (!errs)
		heart_rate_dev_set_handler(handleHeartRate, this);

	return 0 - errs;
}

int HeartRateSensorIngenic::getFd(void) {
	return pollfd;
}

void HeartRateSensorIngenic::handleHeartRate(void *handler, int heartRate) {
	HeartRateSensorIngenic *m = (HeartRateSensorIngenic *)handler;

	if (!m->mInited)
		return;

	m->mHeartRate = heartRate;
	write(m->wakefd, "wakeup", strlen("wakeup") + 1);
}

int HeartRateSensorIngenic::readEvents(sensors_event_t* event, int count) {
	if (!mEnabled || count <= 0) {
		return -EINVAL;
	}

	event->sensor = sSensor.handle;
	event->type = sSensor.type;
	event->heart_rate.bpm = mHeartRate;

	return 1;
}

int HeartRateSensorIngenic::setDelay(int handle, int64_t ns) {
	return 0;
}

int HeartRateSensorIngenic::enable(int handle, int enable) {
	int ret = 0;

	if (!mInited) {
		return -EINVAL;
	}

	if (enable && !mEnabled) {
		ret = heart_rate_dev_set_power(enable);
		if (ret) {
			pr_err("failed to set heart rate dev power enale\n");
		}
	} else if (!enable && mEnabled) {
		ret = heart_rate_dev_set_power(enable);
		if (ret) {
			pr_err("failed to set heart rate dev power disable\n");
		}
	}

	if (!ret) {
		mEnabled = !!enable;
	}

	return ret;
}

bool HeartRateSensorIngenic::isExist()
{
	return mInited;
}

int HeartRateSensorIngenic::populateSensorList(sensor_t *list) {
	memcpy(list, &sSensor, sizeof(sSensor));
	return 1;
}
