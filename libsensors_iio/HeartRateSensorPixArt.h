#ifndef HEART_RATE_SENSOR_H
#define HEART_RATE_SENSOR_H

#include <stdint.h>
#include <errno.h>
#include <sys/cdefs.h>
#include <sys/types.h>
#include <pthread.h>
#include <utils/RefBase.h>

#include "SensorListener.h"
#include "sensors.h"
#include "SensorBase.h"
#include "InputEventReader.h"
#include "sensor_params.h"
#include "algorithm/AlgHeartRatePixArt.h"

#include "frizzioctl.h"

#define PIXART_DEVICE_PATH "/dev/pixart_ofn"
#define NS_TO_MS(n) (n / (1000 * 1000))
#define PIXART_DEFAULT_DELAY (30000000) //30ms

#define PIXART_IOC_MAGIC 'h'
#define PIXART_IOCTL_ENABLE_DISABLE (0)
#define PIXART_IOC_MAXNR 1

#define PIXART_IOCTL_SENSOR_ENABLE_DISABLE \
            _IOW(PIXART_IOC_MAGIC, PIXART_IOCTL_ENABLE_DISABLE, int)

#define EX_PARAM 127
#define FULL_BIT 16
#define AXIS_SIZE 3
#define SENSITIVITY -4  //Gsensor full scale is 16G, 2^4 = 16

#define HEART_RATE_AVALIABLE_LEVEL  (40)
#define HEART_RATE_TIME_PARAM       (100/3)
#define HEART_RATE_OVERTIME         (15 * HEART_RATE_TIME_PARAM)   //15s

enum heart_rate_state {
    STATUS_UNMEASURED,
    STATUS_MEASURING,
    STATUS_MEASURE_TIME_OUT,
    STATUS_MEASURED,
    STATUS_UNKNOWN,
    STATUS_NUMS
};

using namespace android;

class HeartRateSensorPixArt : public SensorBase
{
public:
    HeartRateSensorPixArt();
    virtual ~HeartRateSensorPixArt();

    virtual int readEvents(sensors_event_t* event, int count);
    virtual int setDelay(int64_t ns);
    virtual int enable(int enabled);
    virtual int getFd() const;

    bool isExist() const;
    int populateSensorList(sensor_t* list) const;
    void transGSensorData();
    void reverse_transGSensorData();

private:
    bool initialize();
    int gsensor_fd;
    static void* threadLoop(void* param);
    int enableDisable(int enable);
    void msleep(long long msec) const;
    void connectToSensorService();
    void readGSensor();
    void do_transGSensorData(float *gsensorData);
    int get_sign(int num);
    int get_exponent(int num);
    float get_mantissa(int num);
    float _2pow(int num);
    void two_complement(int *data);
    float do_reverse_transGSensorData(float gsensorData);

    bool mExit;
    bool mExist;
    bool mEnabled;
    int64_t mDelay;
    int mMainReadFd;
    int mMainWriteFd;
    int mWorkerReadFd;
    int mWorkerWriteFd;
    bool mStartThread;
    int mHeartRate;
    int mHeartRateStatus;
    bool mStatusChanged;
    bool mTouchStatus;
    bool mGradeChanged;
    const char* mSensorName;
    AlgHeartRatePixArt* mAlgHeartRatePixArt;
    ppg_mems_data_t mRawData;
    float mGSensorData[3];

    sp<SensorListener> mSensorListener;
	int read_gsensor_data();
	int open_gsensor_device(const char* dev_name);

    pthread_mutex_t mLock;
    pthread_cond_t mCond;
};
#endif //HEART_RATE_SENSOR_H
