#ifndef __FRIZZ_IOCTL_H__
#define __FRIZZ_IOCTL_H__

#include <stdint.h>
#include <errno.h>
#include <sys/cdefs.h>
#include <sys/types.h>
#include <math.h>

#include <linux/input.h>

#include <hardware/hardware.h>
#include <hardware/sensors.h>

enum {
    FRIZZ_OFFSET_EMPTY = 0,

    FRIZZ_OFFSET_ACCELEROMETER = 1,
    FRIZZ_OFFSET_MAGNETIC_FIELD,
    FRIZZ_OFFSET_ORIENTATION,
    FRIZZ_OFFSET_GYROSCOPE,
    FRIZZ_OFFSET_LIGHT,
    FRIZZ_OFFSET_PRESSURE,
    FRIZZ_OFFSET_TEMPERATURE,
    FRIZZ_OFFSET_PROXIMITY,
    FRIZZ_OFFSET_GRAVITY,
    FRIZZ_OFFSET_LINEAR_ACCELERATION,
    FRIZZ_OFFSET_ROTATION_VECTOR,
    FRIZZ_OFFSET_RELATIVE_HUMIDITY,
    FRIZZ_OFFSET_AMBIENT_TEMPERATURE,
    FRIZZ_OFFSET_MAGNETIC_FIELD_UNCALIBRATED,
    FRIZZ_OFFSET_GAME_ROTATION_VECTOR,
    FRIZZ_OFFSET_GYROSCOPE_UNCALIBRATED,
    FRIZZ_OFFSET_SIGNIFICANT_MOTION,
    FRIZZ_OFFSET_STEP_DETECTOR,
    FRIZZ_OFFSET_STEP_COUNTER,
    FRIZZ_OFFSET_GEOMAGNETIC_ROTATION_VECTOR,

    FRIZZ_OFFSET_PDR,
    FRIZZ_OFFSET_MAGNET,
    FRIZZ_OFFSET_GYRO,
    FRIZZ_OFFSET_ACCEL_POWER,
    FRIZZ_OFFSET_ACCEL_LPF,
    FRIZZ_OFFSET_ACCEL_LINEAR,
    FRIZZ_OFFSET_MAGNET_PARAMETER,
    FRIZZ_OFFSET_MAGNET_CALIB_SOFT,
    FRIZZ_OFFSET_MAGNET_LPF,
    FRIZZ_OFFSET_GYRO_LPF,
    FRIZZ_OFFSET_DIRECTION,
    FRIZZ_OFFSET_POSTURE,
    FRIZZ_OFFSET_ROTATION_MATRIX,
    FRIZZ_OFFSET_VELOCITY,
    FRIZZ_OFFSET_RELATIVE_POSITION,
    FRIZZ_OFFSET_MIGRATION_LENGTH,
    FRIZZ_OFFSET_CYCLIC_TIMER,
    FRIZZ_OFFSET_DEBUG_QUEUE_IN,
    FRIZZ_OFFSET_DEBUG_STD_IN,
    FRIZZ_OFFSET_ISP,
    FRIZZ_OFFSET_ACCEL_FALL_DOWN,
    FRIZZ_OFFSET_ACCEL_POS_DET,
    FRIZZ_OFFSET_PDR_GEOFENCING,
    FRIZZ_OFFSET_GESTURE,
    FRIZZ_OFFSET_ACTIVITY,
} ;

#define IOC_MAGIC '9'
#define FRIZZ_IOCTL_SENSOR   1  //sensor関係のIOCTLスタート位置
#define FRIZZ_IOCTL_HARDWARE 64 //frizzのチップ操作に関するIOCTLスタート位置

typedef struct {
    int type; //センサータイプ /hardware/libhardware/include/hardware/sensors.hを参照
    int flag; //0:センサーを無効にする 1:センサーを有効にする
} sensor_enable_t;

#define FRIZZ_IOCTL_SENSOR_SET_ENABLE _IOW(IOC_MAGIC, FRIZZ_IOCTL_SENSOR, sensor_enable_t*)
#define FRIZZ_IOCTL_SENSOR_GET_ENABLE _IOR(IOC_MAGIC, FRIZZ_IOCTL_SENSOR + 1, sensor_enable_t*)

typedef struct {
    int type; //センサータイプ /hardware/libhardware/include/hardware/sensors.hを参照
    int ms;   //ミリ秒
} sensor_delay_t;

#define FRIZZ_IOCTL_SENSOR_SET_DELAY _IOW(IOC_MAGIC, FRIZZ_IOCTL_SENSOR + 2, sensor_delay_t*)
#define FRIZZ_IOCTL_SENSOR_GET_DELAY _IOR(IOC_MAGIC, FRIZZ_IOCTL_SENSOR + 3, sensor_delay_t*)

typedef struct {
    int code;
    struct timeval time;
    union {
        uint32_t u32_value;
        float f32_value[6];
        uint64_t u64_value;
        struct { //for PDR
            float rpos[2];
            float velo[2];
            float total_dst;
            unsigned int count;
        };
    };
} sensor_data_t;

#define FRIZZ_IOCTL_SENSOR_GET_DATA _IOR(IOC_MAGIC, FRIZZ_IOCTL_SENSOR + 4, sensor_data_t*)

typedef struct {
    uint32_t updateflag1;  // flag Word1
    uint32_t updateflag2;  // flag Word2
} sensor_data_changed_t;

#define FRIZZ_IOCTL_GET_SENSOR_DATA_CHANGED _IOR(IOC_MAGIC, FRIZZ_IOCTL_SENSOR + 8, sensor_data_changed_t*)

typedef struct {
    uint32_t version;
} firmware_version_t;

#define FRIZZ_IOCTL_GET_FIRMWARE_VERSION    _IOR(IOC_MAGIC, FRIZZ_IOCTL_SENSOR + 9, firmware_version_t*)

#define FRIZZ_FIFO_FULL_LEAST_BUFER_TIME 10000000000LL // fifo_full使用時の最低バッファ時間

//frizz FW関係の設定
#define FRIZZ_IOCTL_HARDWARE_RESET              _IOW(IOC_MAGIC, FRIZZ_IOCTL_HARDWARE,     void*)
#define FRIZZ_IOCTL_HARDWARE_STALL              _IOW(IOC_MAGIC, FRIZZ_IOCTL_HARDWARE + 1, void*)
#define FRIZZ_IOCTL_HARDWARE_DOWNLOAD_FIRMWARE  _IOW(IOC_MAGIC, FRIZZ_IOCTL_HARDWARE + 2, char*)
#define FRIZZ_IOCTL_HARDWARE_ENABLE_GPIO        _IOW(IOC_MAGIC, FRIZZ_IOCTL_HARDWARE + 3, void*)

#endif
