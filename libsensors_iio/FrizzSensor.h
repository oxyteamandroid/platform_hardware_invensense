
#ifndef __ANDROID_FRIZZ_SENSOR_H__
#define __ANDROID_FRIZZ_SENSOR_H__

#include "SensorBase.h"
#include "frizzioctl.h"

class FrizzSensor : public SensorBase {

public:
    /* HAL type (Android/Iwds) */
    typedef enum {
        FRIZZ_GENERAL_HAL = 1,
        FRIZZ_SPECIAL_HAL,
    } frizz_hal_t;

    typedef int (FrizzSensor::*hfunc_t)(sensors_event_t*);

    struct frizz_sensor_t {
        int handler;
        int offset;
        hfunc_t func;
        bool generals;
        bool specials;

        struct sensor_t *list;
    };

    FrizzSensor(frizz_hal_t type);
    ~FrizzSensor();

    const char *fw_path = "/etc/from.bin";

    int activate(int handle, int enabled);

    int setDelay(int handle, int64_t ns);
    int setRightHand(int32_t handle, int isRightHand);

    int readEvents(sensors_event_t* data, int count);
    bool isEnabled(void);

    int getFd(void);
    int populateSensorList(struct sensor_t *list);

    int create_thread(void);
    int cancal_thread(void);

private:

    int64_t mStepSensorTimestamp;
    uint64_t mLastStepCount;
    uint32_t mLastPDRStepCount;
    struct frizz_sensor_t FrizzSensorList[SENSORS_ID_FRIZZ_NUM];

    int gpoll;
    int gwake;

    int tpoll;
    int twake;

    uint32_t mEnabled;

    frizz_hal_t halType;
    bool thread_run;

    static void *poll_thread(void *arg);

    int sync_event(void);
    struct frizz_sensor_t *getSensorList(int handle);
    int initSensorList(void);

    int getSensorId(struct sensor_t *p);

    /* for driver */
    uint32_t hw_getchange(sensor_data_changed_t *change);
    uint32_t hw_getversion(void);
    uint32_t hw_updateFirmware(const char *);

    int hw_getEnable(sensor_enable_t *sensor_enable);
    int hw_updateEnable(sensor_enable_t *sensor_enable);
    int hw_updateDelay(sensor_delay_t *sensor_delay);
    int hw_readSensorEvent(sensor_data_t *event , int type);
    int hw_readSpecialSensorEvent_activity(sensors_event_t* data, int sensor_offset);

    int hw_readAccelerometerHandler(sensors_event_t *data);
    int hw_readMagneticFieldHandler(sensors_event_t *data);
    int hw_readOrientationHandler(sensors_event_t *data);
    int hw_readGyroscopeHandler(sensors_event_t *data);
    int hw_readPressureHandler(sensors_event_t *data);
    int hw_readGravityHandler(sensors_event_t *data);
    int hw_readLinearAccHandler(sensors_event_t *data);
    int hw_readRotationVectorHandler(sensors_event_t *data);
    int hw_readMagneticFieldUncalibratedHandler(sensors_event_t *data);
    int hw_readGameRotationVectorHandler(sensors_event_t *data);
    int hw_readGyroscopeUncalibratedHandler(sensors_event_t *data);
    int hw_readSignificantMotionHandler(sensors_event_t *data);
    int hw_readStepDetectorHandler(sensors_event_t *data);
    int hw_readStepCounterHandler(sensors_event_t *data);
    int hw_readGeomagneticRotationVectorHandler(sensors_event_t *data);
    int hw_readFifoEmptyHandler(sensors_event_t *data);
    int hw_readPDRStepCounterHandler(sensors_event_t *data);
    int hw_readLigthHandler(sensors_event_t *data);
    int hw_readProximityHandler(sensors_event_t *data);
    int hw_readGestureHandler(sensors_event_t *data);
    int hw_readMotineHandler(sensors_event_t *data);
};

#endif
