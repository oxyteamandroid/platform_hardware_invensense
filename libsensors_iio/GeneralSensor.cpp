/*
 * Copyright (C) 2008 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <fcntl.h>
#include <errno.h>
#include <math.h>
#include <poll.h>
#include <unistd.h>
#include <dirent.h>
#include <sys/select.h>
#include <dlfcn.h>

#include <cutils/log.h>

#include "GeneralSensor.h"
#include "GeneralSensorManager.h"
#include <hardware/sensors.h>

GeneralSensor::GeneralSensor(const char* basePath, general_sensor_desc_t* sensor_desc)
	: SensorBase(NULL, sensor_desc->sensor_name), mSensor(sensor_desc),
	  mGeneralInputReader(32), mTimestamp(0),
	  eventType(sensor_desc->event_type),
	  eventCode(sensor_desc->event_code),
	  sensorHandle(sensor_desc->sensor_handle),
	  sensorType(sensor_desc->sensor_type)
{
    LOGI("%s: begin", mSensor->sensor_name);
    mDelay = -1;

    if (data_fd != -1) {
	    setDevicePath(basePath, mSensor->sys_dev_path);
    } else {
	    input_sysfs_path[0] = '\0';
	    input_sysfs_path_len = 0;
    }
}

GeneralSensor::~GeneralSensor()
{
    LOGI("%s: end", mSensor->sensor_name);
    if (mEnabled != 0) {
	    enable(0);
	    mSensor = NULL;
    }
}

int GeneralSensor::setDevicePath(const char *bpath, const char *rpath)
{
	sprintf(input_sysfs_path, "%s/%s/", bpath, rpath);
	input_sysfs_path_len = strlen(input_sysfs_path);
	LOGI("device path: %s", &input_sysfs_path);
	return 0;
}

int GeneralSensor::enable(int enabled)
{
	int err = 0;
	char buffer[2];

	strcpy(&input_sysfs_path[input_sysfs_path_len], mSensor->sensor_events.enable);

	buffer[0] = enabled ? '1' : '0';
	buffer[1] = '\0';

	err = write_sys_attribute(input_sysfs_path, buffer, 1);
	ALOGD("%s: enable() path:%s", mSensor->sensor_name,
                       &input_sysfs_path);

	if (err != 0) {
		return err;
	}
	ALOGD("%s: set %s to %s", mSensor->sensor_name,
	      &input_sysfs_path[input_sysfs_path_len], buffer);

	int64_t delay = 0;

	if (buffer[0] == '1') {

	    switch (sensorType) {
	    case SENSOR_TYPE_HEART_RATE:
            delay = HEART_RATE_DEFAULT_DELAY;
	        break;
	    case SENSOR_TYPE_STEP_COUNTER:
	        delay = STEP_COUNTER_DEFAULT_DELAY;
	        break;
	    case SENSOR_TYPE_AMBIENT_TEMPERATURE:
	        delay = TEMP_DEFAULT_DELAY;
	        break;
	    case SENSOR_TYPE_RELATIVE_HUMIDITY:
	        delay = HUMI_DEFAULT_DELAY;
	        break;
	    case SENSOR_TYPE_PRESSURE:
	        delay = PRESSURE_DEFAULT_DELAY;
	        break;
	    case SENSOR_TYPE_UV:
	        delay = UV_DEFAULT_DELAY;
	        break;
        case SENSOR_TYPE_PROXIMITY:
            delay = PROXIMITY_DELAY;
            break;

	    default:
	        delay = GENERAL_DEFAULT_DELAY;
	        break;
	    }

        setDelay(delay);
	}

	mEnabled = enabled;

    return err;
}

int GeneralSensor::setDelay(int64_t ns)
{
	VFUNC_LOG;
	int err = 0;
	char buffer[32];
	int bytes;
	int32_t temp = NS_TO_MS(ns);

	strcpy(&input_sysfs_path[input_sysfs_path_len], mSensor->sensor_events.delay);

	bytes = sprintf(buffer, "%ld", temp);
	err = write_sys_attribute(input_sysfs_path, buffer, bytes);
	if (err == 0) {
		mDelay = temp;
		LOGI("%s:: set %s to %ld ms.", mSensor->sensor_name,
		      &input_sysfs_path[input_sysfs_path_len], temp);
	}

    return err;
}

const char* GeneralSensor::getName(void)
{
	return mSensor->sensor_name;
}

general_sensor_desc_t* GeneralSensor::getSensorDesc(void)
{
	return mSensor;
}

int GeneralSensor::readEvents(sensors_event_t *data, int count)
{
	int done = 0;
	if (!mEnabled || count <= 0) {
		return 0;
	}

	ssize_t n = mGeneralInputReader.fill(data_fd);
	if (n < 0) {
		return n;
	}

	data->version = sizeof(sensors_event_t);

	input_event const* event;

	while (done == 0 && mGeneralInputReader.readEvent(&event)) {
		int type = event->type;
		int code = event->code;

		data->sensor  = getSensorHandle();
		data->type    = getSensorType();
		bool has_data = 
			getEventCode() == NULL_VALUE ? 
			type == getEventType() : 
			(type == getEventType() && code == getEventCode());

		if (has_data) {
			data->data[0] = (float)event->value;
			mTimestamp = timevalToNano(event->time);
			done = 1;
			count--;
#ifdef DEBUG
			LOGI("%s sensor value: %f\n", getName(), data->data[0]);
#endif
		} else if (type == EV_SYN) {
			data->timestamp = mTimestamp;
		}

		mGeneralInputReader.next();
	}
	return done;
}
