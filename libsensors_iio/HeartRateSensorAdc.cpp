#include <fcntl.h>
#include <errno.h>
#include <math.h>
#include <poll.h>
#include <unistd.h>
#include <dirent.h>
#include <sys/select.h>
#include <dlfcn.h>

#include <cutils/log.h>
#include "HeartRateSensorAdc.h"

static sensor_t sSensor = {
        "Heart rate sensor","Ingenic", 1,
        SENSORS_ADC_HEART_RATE_HANDLE,
        SENSOR_TYPE_HEART_RATE,
        1100.0f,
        0.005f,
        0.005f,
        10000,
		0, 0,
		SENSOR_STRING_TYPE_HEART_RATE,
		"", 10000,
		SENSOR_FLAG_CONTINUOUS_MODE,
        { }
};

heart_rate_desc_t HeartRateSensorAdc::heart_rate_sensor_map[] = {
    {
        "ADC_heart_rate",
        "/sys/class/hwmon/hwmon0/device",
        {
            "enable_heart",
            "poll_period_ms_heart"
        },
        EV_ABS,
        NULL_VALUE,
        SENSORS_ADC_HEART_RATE_HANDLE,
        SENSOR_TYPE_HEART_RATE
    },

    {
        "ADC_heart_rate",
        "/sys/class/hwmon/hwmon1/device",
        {
            "enable_heart",
            "poll_period_ms_heart"
        },
        EV_ABS,
        NULL_VALUE,
        SENSORS_ADC_HEART_RATE_HANDLE,
        SENSOR_TYPE_HEART_RATE
    }
};

HeartRateSensorAdc::HeartRateSensorAdc()
        : SensorBase(NULL, NULL),
          mMainReadFd(-1),
          mMainWriteFd(-1),
          mWorkerReadFd(-1),
          mWorkerWriteFd(-1),
          mEnabled(false),
          mExist(false),
          mDesc(NULL),
          mGeneralInputReader(32),
          mTimestamp(0),
          mDelay(-1),
          mAlgHeartRateAdc(NULL),
          mStartThread(false),
          mHeartRate(-1)
{
    mExist = scanSensor();
    if (mExist) {
        data_fd = openInput(mDesc->name);
        if ((data_fd < 0) || !intialize()) {
            mExist = false;
        }
    } else {
        LOGI("No ADC Heart Rate sensor!");
    }
}

HeartRateSensorAdc::~HeartRateSensorAdc()
{
    if (mExist) {
        LOGI("%s: destruct", mDesc->name);

        mStartThread = false;
        char msg = 'h';
        int retval = write(mWorkerWriteFd, &msg, 1);
        if (retval)
            LOGE("Failed to write pipe: %s", strerror(errno));

        close(mMainReadFd);
        close(mMainWriteFd);

        close(mWorkerReadFd);
        close(mWorkerWriteFd);

        if (mEnabled) {
            enable(0);
            mDesc = NULL;
        }
    }
}

bool HeartRateSensorAdc::intialize()
{
    int retval = 0;

    sprintf(input_sysfs_path, "%s/", mDesc->sysPath);
    input_sysfs_path_len = strlen(input_sysfs_path);
    LOGI("device path: %s", &input_sysfs_path);

    int pipeFd0[2];
    retval = pipe(pipeFd0);
    if (retval < 0) {
        LOGE("Failed to create pipe: %s", strerror(errno));
        return false;
    }
    mMainReadFd = pipeFd0[0];
    mWorkerWriteFd = pipeFd0[1];

    int pipeFd1[2];
    retval = pipe(pipeFd1);
    if (retval < 0) {
        LOGE("Failed to create pipe: %s", strerror(errno));
        return false;
    }
    mWorkerReadFd = pipeFd1[0];
    mMainWriteFd = pipeFd1[1];

    mAlgHeartRateAdc = AlgHeartRateAdc::getInstance();

    pthread_t tid;
    pthread_attr_t attr;
    pthread_attr_init(&attr);
    pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_DETACHED);

    mStartThread = true;
    int ret = pthread_create(&tid, &attr, threadLoop, this);
    if (ret) {
        ALOGE("Failed to create poll thread %s", strerror(ret));
        return false;
    }

    pthread_attr_destroy(&attr);

    return true;
}

void *HeartRateSensorAdc::threadLoop(void *param)
{
    HeartRateSensorAdc* hs = reinterpret_cast<HeartRateSensorAdc *>(param);

    struct pollfd fds[2];

    fds[0].fd = hs->data_fd;
    fds[0].events = POLLIN;
    fds[0].revents = 0;

    fds[1].fd = hs->mWorkerReadFd;
    fds[1].events = POLLIN;
    fds[1].revents = 0;

    struct sensors_event_t event;
    int count = 0;
    int retval = 0;
    int heartRate = 0;

    while(hs->mStartThread) {

        do {
            count = poll(fds, 2, -1);
        } while(count < 0 && errno == EINTR);

        if (fds[0].revents & POLLIN) {
            fds[0].revents = 0;
            retval = hs->readInput(&event, 1);
            if (!retval) {
                LOGE("Failed to read event: %s", strerror(errno));
                continue;
            }

            hs->mAlgHeartRateAdc->handleEvent(&event);
            heartRate = hs->mAlgHeartRateAdc->getHeartRate();

            if (heartRate > 0) {
                hs->mHeartRate = heartRate;
                char msg = 'h';
                retval = write(hs->mWorkerWriteFd, &msg, 1);
                if (retval < 0) {
                    LOGE("Failed to write msg to pipe: %s", strerror(errno));
                    continue;
                }
            }
        }

        if (fds[1].revents & POLLIN) {
            fds[1].revents = 0;
            char msg;
            retval = read(fds[1].fd, &msg, 1);
            if (retval < 0) {
                LOGE("Failed to read pipe: %s", strerror(errno));
                continue;
            }
        }
    }

    return NULL;
}

int HeartRateSensorAdc::readEvents(sensors_event_t* event, int count)
{
    if (count < 1) {
        return -EINVAL;
    }

    event->sensor = mDesc->handle;
    event->type = mDesc->type;
    event->heart_rate.bpm = mHeartRate;

    return 1;
}

bool HeartRateSensorAdc::scanSensor()
{
    char dirName[PATH_MAX] = {0};
    DIR *dir = NULL;
    int adapter = 0;
    struct dirent *de;

    for (int i = 0; i < ARRAY_SIZE(heart_rate_sensor_map); i++) {
        sprintf(dirName, "%s", heart_rate_sensor_map[i].sysPath);
        LOGI("%s", dirName);
        dir = opendir(dirName);
        if (dir == NULL)
            continue;

        while((de = readdir(dir))) {
            if(de->d_name[0] == '.' &&
               (de->d_name[1] == '\0' || (de->d_name[1] == '.' && de->d_name[2] == '\0'))) {
                continue;
            }

            for(int j = 0; j < 2; j++) {
                if (!strcmp(de->d_name, heart_rate_sensor_map[i].ctrlDesc.enable) ||
                    !strcmp(de->d_name, heart_rate_sensor_map[i].ctrlDesc.delay)) {
                    adapter++;
                }
            }
        }
        closedir(dir);

        if (adapter >= 2) {
            LOGI("%s is selected.", heart_rate_sensor_map[i].name);
            mDesc = &heart_rate_sensor_map[i];
            return true;
        }
    }
    return false;
}

int HeartRateSensorAdc::populateSensorList(struct sensor_t *list)
{
    memcpy(list, &sSensor, sizeof(struct sensor_t));
    return 1;
}

int HeartRateSensorAdc::enable(int enabled)
{
    int err = 0;
    char buffer[2];

    buffer[0] = enabled ? '1' : '0';
    buffer[1] = '\0';

    strcpy(&input_sysfs_path[input_sysfs_path_len], mDesc->ctrlDesc.enable);

    err = write_sys_attribute(input_sysfs_path, buffer, 1);
    ALOGD("%s: enable() path:%s", mDesc->name, &input_sysfs_path);

    if (err != 0) {
        return err;
    }
    ALOGD("%s: set %s to %s", mDesc->name,
            &input_sysfs_path[input_sysfs_path_len], buffer);

    setDelay(HEART_RATE_DEFAULT_DELAY);

    if (enabled)
        mEnabled = true;
    else
        mEnabled = false;

    return 0;
}

int HeartRateSensorAdc::setDelay(int64_t ns)
{
    VFUNC_LOG;
    int err = 0;
    char buffer[32];
    int bytes;
    int32_t temp = NS_TO_MS(ns);

    strcpy(&input_sysfs_path[input_sysfs_path_len], mDesc->ctrlDesc.delay);

    bytes = sprintf(buffer, "%d", temp);
    err = write_sys_attribute(input_sysfs_path, buffer, bytes);
    if (err == 0) {
        mDelay = temp;
        LOGI("%s:: set %s to %d ms.", mDesc->name,
              &input_sysfs_path[input_sysfs_path_len], temp);
    }

    return err;
}

int HeartRateSensorAdc::readInput(sensors_event_t *data, int count)
{
    int done = 0;
    if (!mEnabled || count <= 0) {
        return -EINVAL;
    }

    ssize_t n = mGeneralInputReader.fill(data_fd);
    if (n < 0) {
        return n;
    }

    data->version = sizeof(sensors_event_t);

    input_event const* event;

    while (done == 0 && mGeneralInputReader.readEvent(&event)) {
        int type = event->type;
        int code = event->code;

        data->sensor  = mDesc->handle;
        data->type    = mDesc->type;
        bool has_data =
            mDesc->eventCode == NULL_VALUE ?
            type == mDesc->eventType :
            (type == mDesc->eventType && code == mDesc->eventCode);

        if (has_data) {
            data->data[0] = (float)event->value;
            mTimestamp = timevalToNano(event->time);
            done = 1;
            count--;
#ifdef DEBUG
            LOGI("%s sensor value: %f\n", mDesc->name, data->data[0]);
#endif
        } else if (type == EV_SYN) {
            data->timestamp = mTimestamp;
        }

        mGeneralInputReader.next();
    }
    return done;
}

int HeartRateSensorAdc::getFd() const
{
    return mMainReadFd;
}

bool HeartRateSensorAdc::isExist() const
{
    return mExist;
}
