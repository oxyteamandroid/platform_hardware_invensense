#ifndef _HEARTRATESENSORINGENIC_H_
#define _HEARTRATESENSORINGENIC_H_

#include "SensorBase.h"
#include "algorithm/ingenic_heart_rate.h"

class HeartRateSensorIngenic : public SensorBase {
private:
	int pollfd;
	int wakefd;
	int mInited;
	int mEnabled;
	int mHeartRate;

public:
	HeartRateSensorIngenic(void);
	~HeartRateSensorIngenic(void);
	int getFd(void);
	int readEvents(sensors_event_t* event, int count);
	int setDelay(int handle, int64_t ns);
	int enable(int handle, int enable);
	bool isExist(void);
	int populateSensorList(sensor_t *list);

private:
	int initialize(void);
	static void handleHeartRate(void *handler, int heartRate);
};

#define pr_err(fmt, args...) ALOGE(fmt, ##args)
#define pr_info(fmt, args...) ALOGE(fmt, ##args)

#endif /* _HEARTRATESENSORINGENIC_H_ */
