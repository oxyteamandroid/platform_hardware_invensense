#!/bin/bash

# This is a sample of the command line make used to build
#   the libraries and binaries for the Pandaboard.
# Please customize this path to match the location of your
#   Android source tree. Other variables may also need to
#   be customized such as:
#     $CROSS, $PRODUCT, $KERNEL_ROOT

# this function comes from build/envsetup.sh
function gettop
{
    local TOPFILE=build/core/envsetup.mk
    if [ -n "$TOP" -a -f "$TOP/$TOPFILE" ] ; then
        echo $TOP
    else
        if [ -f $TOPFILE ] ; then
            # The following circumlocution (repeated below as well) ensures
            # that we record the true directory name and not one that is
            # faked up with symlink names.
            PWD= /bin/pwd
        else
            # We redirect cd to /dev/null in case it's aliased to
            # a command that prints something as a side-effect
            # (like pushd)
            local HERE=$PWD
            T=
            while [ \( ! \( -f $TOPFILE \) \) -a \( $PWD != "/" \) ]; do
                cd .. > /dev/null
                T=`PWD= /bin/pwd`
            done
            cd $HERE > /dev/null
            if [ -f "$T/$TOPFILE" ]; then
                echo $T
            fi
        fi
    fi
}

ANDROID_BASE=$(gettop)
PRODUCT_NAME=$1
CMD=$2

if [ ${PRODUCT_NAME} == "" ] ; then
	echo "Usage :"
	echo ". build.sh product_name command "
	echo "command include clean cleanall or empty"
	echo "Examples :"
	echo ". build.sh s2124b_15"
	echo ". build.sh s2124b_15 clean"
	echo ". build.sh s2124b_15 cleanall"
else
make ${CMD} -C ${ANDROID_BASE}/hardware/invensense/libsensors_iio/software/build/android \
	VERBOSE=0 \
	TARGET=android \
	ANDROID_ROOT=${ANDROID_BASE} \
	KERNEL_ROOT=${ANDROID_BASE}/kernel \
	CROSS=${ANDROID_BASE}/prebuilts/gcc/linux-x86/mips/mipsel-linux-android-${TARGET_GCC_VERSION}/bin/mipsel-linux-android- \
	PRODUCT=${PRODUCT_NAME} \
	MPL_LIB_NAME=mplmpu \
	echo_in_colors=echo \
	-f shared.mk
fi
