/*
 * Copyright (C) 2011 Invensense, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef INV_HAL_SYS_PED_H
#define INV_HAL_SYS_PED_H

#include "sensors.h"
#include "SensorBase.h"
#include "MPLSensor.h"
#include <gui/MplInterfaces.h>


class MPLSensorSysPed
            : public MPLSensor,
              public MplSysPed_Interface
{
public:
    MPLSensorSysPed(CompassSensor *);
    virtual ~MPLSensorSysPed();

    virtual int rpcStartPed();
    virtual int rpcStopPed();
    virtual int rpcGetSteps();
    virtual long rpcGetWalkTime();
    virtual int rpcClearPedData();

protected:

    /* internal logic */
    virtual int inv_clear_pedo_data(void);
    virtual int inv_clear_dmp_pedo_data(void);
	virtual int inv_set_pedo_parameters(void);
    virtual int inv_get_dmp_pedo_walktime(long long *walktime);
    virtual int inv_get_dmp_pedo_steps(int *steps);

private:
    int mCachedSteps;
    long long mCachedWalktime;

    struct pedo_sysfs_attrbs {
       char *steps;
       char *walk_time;
	   char *clip_threshold;
	   char *min_steps;
	   char *max_step_buffer_time;
	   char *threshold;
	   char *min_up_time;
	   char *max_up_time;
	   char *min_energy;
    } mpu_pedo;
    char *pedo_sysfs_names_ptr;

    int inv_init_pedo_sysfs_attributes(void);
};

extern "C" {
MplSysPed_Interface* getSysPedInterfaceObject();
}

#endif /* INV_HAL_SYS_PED_H */

