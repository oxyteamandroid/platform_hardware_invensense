#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <inttypes.h>

#include <cutils/log.h>
#include <hardware/sensors.h>

int sensorTest(int index, int delay, int times)
{
    int err, i;
    int count;

    struct sensor_t const* list;
    struct sensor_t const *listTest;
    struct sensors_poll_device_t* device;
    struct sensors_module_t* module;

    if (times == 0)
        return 0;

    if (delay == 0)
        return 0;

    // -----------------
    // get hal object
    // -----------------
    err = hw_get_module(SENSORS_HARDWARE_MODULE_ID,
            (hw_module_t const**) &module);

    // -----------------
    // open test
    // -----------------
    err = sensors_open(&module->common, &device);

    // -----------------
    // get list test
    // -----------------
    count = module->get_sensors_list(module, &list);

    if (count < (index + 1))
        return 0;

    for (i = 0; i < count; i++) {
        printf("get [%d] Sensor list %s\n", i, list[i].name);
    }

    listTest = &list[index];

    printf("Test Sensor %s\n", listTest->name);

    // test one times by ACC
    err = device->activate(device, listTest->handle, 1);

    device->setDelay(device, listTest->handle, delay * 1000000);

    sleep(3);

    printf("Polling ....\n");

    {
        sensors_event_t data[16];
        int cnt = 0;
        int j;
        for (i = 1; i < times;) {
            memset(data, 0, sizeof(data));
            cnt = device->poll(device, data, 16);
            for (j = 0; j < cnt; j ++) {
                if (data[j].type == listTest->type) {
                    i++;
                    printf("Dump time %d pid %d D %d %f %f %f %lld\n", i, getpid(), cnt,
                            data[j].data[j], data[j].data[1], data[j].data[2], data[j].step_counter);
                }
            }
        }
    }

    device->activate(device, listTest->handle, 0);

    sensors_close(device);

    printf("Polling .(%d). END ..\n", getpid());

    return 0;
}

int main(int argc, char *argv[])
{
    int index, opt;
    int delay, times;

    delay = 1000;
    times = 10;
    index = 0;

    while ((opt = getopt(argc, argv, "i:d:t:")) != -1) {
        switch (opt) {
        case 'i':
            index = atoi(optarg);
            break;
        case 'd':
            delay = atoi(optarg);
            break;
        case 't':
            times = atoi(optarg);
            break;
        default: /* '?' */
            fprintf(stderr, "Usage: %s [-i index] [-d delay] [-t times]\n", argv[0]);
            exit(EXIT_FAILURE);
        }
    }

//	if (optind >= argc) {
//		fprintf(stderr, "Expected argument after options\n");
//		exit(EXIT_FAILURE);
//	}

    printf("Test mode index %d delsy:%d(ns) times:%d\n", index, delay, times);

    sensorTest(index, delay, times);

    exit(EXIT_SUCCESS);
}

