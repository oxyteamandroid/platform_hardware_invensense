#include "SensorListener.h"
#include <math.h>
#include <sys/types.h>
#include <stdint.h>

static int sensor_events_listener(int fd, int events, void* data) {
    SensorListener* listener = (SensorListener*) data;
    ASensorEvent sen_events;
    int res;
    res = listener->mSensorEventQueue->read(&sen_events, 1);

    if (res == 0) {
        res = listener->mSensorEventQueue->waitForEvent();
        if (res != NO_ERROR) {
            ALOGE("%s; waitForEvent error", __FUNCTION__);
            return -1;
        }
    }

    if (res <= 0) {
        ALOGE("%s: error", __FUNCTION__);
        return -1;
    }

    if (Sensor::TYPE_ACCELEROMETER == listener->getRegisterSensorType()) {
        float x = sen_events.vector.v[0];
        float y = sen_events.vector.v[1];
        float z = sen_events.vector.v[2];

#if 0
        ALOGI("====================================");
        ALOGI("Raw GSensor X = %f", x);
        ALOGI("Raw GSensor Y = %f", y);
        ALOGI("Raw GSensor Z = %f", z);
        ALOGI("====================================");
#endif

        listener->setAccelerometer(x, y, z);
    }

    return 1;
}

SensorListener::SensorListener() {
    mSensorEnabled = 0;
    mRegisterSensorType = -1;

    /*
     * Initialize default GSensor value
     */
    mAccel[0] = 0.6200982332229614;
    mAccel[1] = 1.655815124511719;
    mAccel[2] = 0.040701430290937424;

    ALOGI("SensorListener construct");
}

SensorListener::~SensorListener() {
    if (mSensorLooperThread.get()) {
        // 1. Request exit
        // 2. Wake up looper which should be polling for an event
        // 3. Wait for exit
        mSensorLooperThread->requestExit();
        mSensorLooperThread->wake();
        mSensorLooperThread->join();
        mSensorLooperThread.clear();
        mSensorLooperThread = NULL;
    }

    if (mLooper.get()) {
        mLooper->removeFd(mSensorEventQueue->getFd());
        mLooper.clear();
        mLooper = NULL;
    }

    ALOGI("SensorListener destroyed");
}

void SensorListener::setAccelerometer(float x, float y, float z)
{
    mAccel[0] = x;
    mAccel[1] = y;
    mAccel[2] = z;
}

float* SensorListener::getAccelerometer()
{
    return mAccel;
}

int SensorListener::initialize() {
    int ret = NO_ERROR;
    SensorManager& mgr(SensorManager::getInstance());

#ifdef DEBUG
    Sensor const* const* sensorList;
    int count = mgr.getSensorList(&sensorList);
    ALOGI("%s: android sensor have %d num", __FUNCTION__, count);
    ALOGI("=========================================================");
    ALOGI("Dump Android Sensor List");
    for (int i = 0; i < count; i++) {
        ALOGI("%s", sensorList[i]->getName().string());
    }
    ALOGI("=========================================================");
#endif

    mSensorEventQueue = mgr.createEventQueue();
    if (mSensorEventQueue == NULL) {
        ALOGE("createEventQueue returned NULL");
        ret = NO_INIT;
        goto out;
    }

    mLooper = new Looper(false);
    mLooper->addFd(mSensorEventQueue->getFd(), 0, ALOOPER_EVENT_INPUT,
            sensor_events_listener, this);

    if (mSensorLooperThread.get() == NULL) {
        mSensorLooperThread = new SensorLooperThread(mLooper.get());
    }

    if (mSensorLooperThread.get() == NULL) {
        ALOGE("Could't create sensor looper thread");
        ret = NO_MEMORY;
        goto out;
    }

    ret = mSensorLooperThread->run("Sensor looper thread",
            PRIORITY_URGENT_DISPLAY);

    if (ret == INVALID_OPERATION) {
        ALOGE("thread already running");
    } else if (ret != NO_ERROR) {
        ALOGE("could't run thread");
        goto out;
    }

out:
    return ret;
}

int SensorListener::getRegisterSensorType(void)
{
    return mRegisterSensorType;
}

int SensorListener::enableSensor(sensor_type_t type) {
    const Sensor* sensor;
    SensorManager& mgr(SensorManager::getInstance());

    AutoMutex lock(mLock);

    if ((type & SENSOR_ACCELEROMETER) &&
            !(mSensorEnabled & SENSOR_ACCELEROMETER)) {

        sensor = mgr.getDefaultSensor(Sensor::TYPE_ACCELEROMETER);

        if (sensor->getType() != Sensor::TYPE_ACCELEROMETER) {
            ALOGE("%s: enable accelerometer error, handle: %d, name: %s",
                    __FUNCTION__, sensor->getHandle(),
                    sensor->getName().string());
            return BAD_VALUE;
        }

        ALOGD("%s:name: %s, type: %d, handle: %d", __FUNCTION__,
                sensor->getName().string(), sensor->getType(),
                sensor->getHandle());

        mSensorEventQueue->enableSensor(sensor);
        mSensorEventQueue->setEventRate(sensor, ms2ns(15));
        mSensorEnabled |= SENSOR_ACCELEROMETER;
        mRegisterSensorType = sensor->getType();
    }

    return NO_ERROR;
}

void SensorListener::disableSensor(sensor_type_t type) {
    const Sensor *sensor;

    SensorManager& mgr(SensorManager::getInstance());

    AutoMutex lock(mLock);

    if ((type & SENSOR_ACCELEROMETER)
            && (mSensorEnabled & SENSOR_ACCELEROMETER)) {

        sensor = mgr.getDefaultSensor(Sensor::TYPE_ACCELEROMETER);
        if (sensor->getType() != Sensor::TYPE_ACCELEROMETER) {
            ALOGE("%s: disable accelerometer error", __FUNCTION__);
            return;
        }

        ALOGD("%s: name: %s, type: %d, handle: %d", __FUNCTION__,
                sensor->getName().string(), sensor->getType(),
                sensor->getHandle());

        mSensorEventQueue->disableSensor(sensor->getHandle());
        mSensorEnabled &= ~SENSOR_ACCELEROMETER;
        mRegisterSensorType = -1;
    }
}
