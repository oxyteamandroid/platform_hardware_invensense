#include <fcntl.h>
#include <errno.h>
#include <math.h>
#include <poll.h>
#include <unistd.h>
#include <dirent.h>
#include <sys/select.h>
#include <dlfcn.h>
#include <cutils/log.h>
#include <hardware/sensors.h>
#include "HeartRateSensorPixArt.h"
#include <math.h>

#define MGSENSOR_DEVICE_PATH "/dev/frizz"

static sensor_t sSensor = {
        "Heart rate sensor",
        "PixArt",
        1,
        SENSORS_PIXART_HEART_RATE_HANDLE,
        SENSOR_TYPE_HEART_RATE,
        1100.0f,
        0.005f,
        0.005f,
        10000,
		0, 0,
		SENSOR_STRING_TYPE_HEART_RATE,
		"", 10000,
		SENSOR_FLAG_CONTINUOUS_MODE,
        { }
};

HeartRateSensorPixArt::HeartRateSensorPixArt()
    : SensorBase(PIXART_DEVICE_PATH, NULL),
      mExit(false),
      gsensor_fd(-1),
      mExist(false),
      mEnabled(false),
      mDelay(-1),
      mMainReadFd(-1),
      mMainWriteFd(-1),
      mWorkerReadFd(-1),
      mWorkerWriteFd(-1),
      mStartThread(false),
      mHeartRate(-1),
      mHeartRateStatus(0),
      mStatusChanged(false),
      mTouchStatus(false),
      mGradeChanged(false),
      mSensorName("PixArt Heart Rate"),
      mAlgHeartRatePixArt(NULL),
      mSensorListener(NULL)
{
    open_device();
    if (dev_fd >= 0) {
        mExist = initialize();
    }

    if (!mExist)
        LOGI("NO PixArt Heart Rate sensor!");
    else
        LOGI("PixArt Heart Rate sensor initialize done.");
}

HeartRateSensorPixArt::~HeartRateSensorPixArt()
{
    if (mExist) {
        LOGI("%s: destruct", mSensorName);

        mStartThread = false;

        close(mMainReadFd);
        close(mMainWriteFd);

        close(mWorkerReadFd);
        close(mWorkerWriteFd);

        mExit = true;
        if (mEnabled)
            enable(0);
    }
}

bool HeartRateSensorPixArt::initialize()
{
    int retval = 0;

    /*
     * setup pipe
     */
    int pipeFd0[2];
    struct sched_param thread_param;

    retval = pipe(pipeFd0);
    if (retval < 0) {
        LOGE("Failed to create pipe: %s", strerror(errno));
        return false;
    }
    mMainReadFd = pipeFd0[0];
    mWorkerWriteFd = pipeFd0[1];

    int pipeFd1[2];
    retval = pipe(pipeFd1);
    if (retval < 0) {
        LOGE("Failed to create pipe: %s", strerror(errno));
        return false;
    }
    mWorkerReadFd = pipeFd1[0];
    mMainWriteFd = pipeFd1[1];

    retval = open_gsensor_device(MGSENSOR_DEVICE_PATH);
    if(retval < 0) {
        LOGE("Failed to open gsensor device : %s \n %s", MGSENSOR_DEVICE_PATH, strerror(errno));
    }
    /*
     * initialize algorithm
     */
    mAlgHeartRatePixArt = AlgHeartRatePixArt::getInstance();

    //Call SetMemsScale(1) if G-Sensor is not 2G
    mAlgHeartRatePixArt->SetMemsScale(1);

    mAlgHeartRatePixArt->EnableFastOutput(0);

    //Select Auto mode or Motion mode
    mAlgHeartRatePixArt->EnableAutoMode(1);
    mAlgHeartRatePixArt->EnableMotionMode(0);

    /*
     * create thread
     */
    pthread_t tid;
    pthread_attr_t attr;
    pthread_attr_init(&attr);
    pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_DETACHED);

    retval = pthread_attr_setschedpolicy(&attr, SCHED_RR);
    if(retval != 0)
        LOGE("Unable to set SCHED_RR policy.  retval = %d", retval);

    thread_param.sched_priority = sched_get_priority_max(SCHED_RR);
    retval = pthread_attr_setschedparam(&attr, &thread_param);
    if(retval)
        LOGE("Unable to set priority %d, retval = %d", thread_param.sched_priority, retval);

//  android do not supports to change inherit, but it still work?
//  retval = pthread_attr_setinheritsched(&attr, PTHREAD_EXPLICIT_SCHED);
//  if(retval)
//      LOGE("Unable to set inheritsched. reval = %d", retval);

    retval = pthread_create(&tid, &attr, threadLoop, this);
    if (retval) {
        ALOGE("Failed to create poll thread %s", strerror(retval));
        return false;
    }

    pthread_attr_destroy(&attr);

    retval = pthread_mutex_init(&mLock, NULL);
    if (retval < 0) {
        LOGE("Failed to init mutex: %s", strerror(retval));
        return false;
    }

    retval = pthread_cond_init(&mCond, NULL);
    if (retval < 0) {
        LOGE("Failed to init condition: %s", strerror(retval));
        return false;
    }

    connectToSensorService();
    return true;
}

void HeartRateSensorPixArt::msleep(long long msec) const {
    struct timespec ts;
    int retval = 0;

    ts.tv_sec = (msec / 1000);
    ts.tv_nsec = (msec % 1000) * 1000 * 1000;

    do {
        retval = nanosleep(&ts, &ts);
    } while (retval < 0 && errno == EINTR);
}

int HeartRateSensorPixArt::getFd() const
{
    return mMainReadFd;
}

void HeartRateSensorPixArt::connectToSensorService()
{
    int retval = 0;
    mSensorListener = new SensorListener();
    if ((mSensorListener != NULL) && mSensorListener.get()) {

        retval = mSensorListener->initialize();
        if (retval != NO_ERROR) {
            mSensorListener.clear();
            mSensorListener = NULL;
        }
    }
}

void HeartRateSensorPixArt::readGSensor()
{
    if (mSensorListener != NULL && mSensorListener.get()) {
        mGSensorData[0] = mSensorListener->getAccelerometer()[0];
        mGSensorData[1] = mSensorListener->getAccelerometer()[1];
        mGSensorData[2] = mSensorListener->getAccelerometer()[2];
    } else {
        mGSensorData[0] = 0.6200982332229614;
        mGSensorData[1] = 1.655815124511719;
        mGSensorData[2] = 0.040701430290937424;
    }
}

void *HeartRateSensorPixArt::threadLoop(void *param)
{
    HeartRateSensorPixArt* hs = reinterpret_cast<HeartRateSensorPixArt *>(param);
    int retval = 0;
    int heartRate = 0;
    int grade = 0;
    int heartRateStatus = -1;
    int n = 0;
    struct pollfd pfs[1];
    int gradeCount = 0;
    bool heartRateChange = false;
    bool touchStatus = false;
    bool startMeasure = true;
    bool countEnable = true;
    bool gradeCountOverflow = false;
    enum heart_rate_state next_state = STATUS_UNKNOWN;
    enum heart_rate_state current_state = STATUS_UNKNOWN;

    pfs[0].fd = hs->gsensor_fd;
    pfs[0].events = POLLIN;
    pfs[0].revents = 0;

    for (;;) {
        pthread_mutex_lock(&hs->mLock);
        while (!hs->mStartThread) {
            if (hs->mExit)
                break;
            /* stop pixart algorithm */
            hs->mAlgHeartRatePixArt->close();

            pthread_cond_wait(&hs->mCond, &hs->mLock);

            /* reset pixart algorithm */
            hs->mAlgHeartRatePixArt->Algorithm_Reset();

            gradeCount = 0;
            startMeasure = true;
            countEnable = true;
            gradeCountOverflow = false;
            heartRateStatus = -1;
            hs->mHeartRateStatus = -1;
            hs->mTouchStatus = false;
        }
        pthread_mutex_unlock(&hs->mLock);

        do {
            n = poll(pfs, 1, -1);
        } while (n < 0 && errno == EINTR);

        if (n < 0) {
            LOGE("Frizz poll_thread fd error quit");
            continue;
        }

        if (pfs[0].revents & POLLIN) {
            pfs[0].revents = 0;
            retval = hs->read_gsensor_data();
            if(retval) {
                continue;
            }
        }

        //First time to start measure should sleep to measure touch status
        if (startMeasure) {
            hs->msleep(300);
            current_state = STATUS_MEASURING;
        }

        retval = read(hs->dev_fd, &hs->mRawData, sizeof(ppg_mems_data_t));
        if (retval < 0) {
            LOGE("Failed to read %s: %s", PIXART_DEVICE_PATH, strerror(errno));
            continue;
        }

        heartRate = hs->mAlgHeartRatePixArt->handleData(&hs->mRawData,
                hs->mGSensorData);
        touchStatus = hs->mAlgHeartRatePixArt->getTouchStatus();
        grade = hs->mAlgHeartRatePixArt->getHeartRateGrade();
        heartRateChange = false;

        if (countEnable)
            gradeCount++;
        if (gradeCount >= HEART_RATE_OVERTIME) {
            gradeCountOverflow = true;
            gradeCount = 0;
        } else {
            gradeCountOverflow = false;
        }

        switch(current_state) {
        case STATUS_UNMEASURED:
            if (touchStatus == true) {
                next_state = STATUS_MEASURING;
                countEnable = true;
                gradeCount = 0;

            } else {
                next_state = STATUS_UNMEASURED;
            }
            heartRateStatus = HEART_RATE_STATUS_UNAVALIABLE;
            hs->mHeartRate = 0;
            break;

        case STATUS_MEASURING:
            if (touchStatus == false) {
                if (startMeasure) {
                    heartRateChange = true;
                    hs->mHeartRate = 0;
                    hs->mStatusChanged = true;
                }
                next_state = STATUS_UNMEASURED;
                heartRateStatus = HEART_RATE_STATUS_UNAVALIABLE;
                countEnable = false;
                gradeCount = 0;

            } else if (gradeCountOverflow) {
                hs->mHeartRate = 0;
                gradeCountOverflow = false;
                next_state = STATUS_MEASURE_TIME_OUT;
                heartRateStatus = HEART_RATE_STATUS_UNAVALIABLE;
                countEnable = false;
                gradeCount = 0;

            } else if (grade >= HEART_RATE_AVALIABLE_LEVEL) {
                if (heartRate >= 0) {
                    hs->mHeartRate = heartRate;
                } else {
                    hs->mHeartRate = 0;
                }

                next_state = STATUS_MEASURED;
                heartRateStatus = HEART_RATE_STATUS_AVALIABLE;
                heartRateChange = true;
                countEnable = false;
                gradeCount = 0;

            } else {
                next_state = STATUS_MEASURING;
            }
            break;

        case STATUS_MEASURE_TIME_OUT:
            if (touchStatus == false) {
                next_state = STATUS_UNMEASURED;

            } else if (grade >= HEART_RATE_AVALIABLE_LEVEL) {
                next_state = STATUS_MEASURED;
                heartRateStatus = HEART_RATE_STATUS_AVALIABLE;

            } else {
                next_state = STATUS_MEASURE_TIME_OUT;
            }
            break;

        case STATUS_MEASURED:
            if (heartRate < 0)
                break;

            if (touchStatus == false) {
                next_state = STATUS_UNMEASURED;
                heartRateStatus = HEART_RATE_STATUS_UNAVALIABLE;
                hs->mHeartRate = 0;

            } else {
                next_state = STATUS_MEASURED;
                if (hs->mHeartRate != heartRate) {
                    heartRateChange = true;
                    hs->mHeartRate = heartRate;
                }
            }
            break;

        default:
            LOGE("HR threadLoop ERORR");
            break;
        }

        current_state = next_state;

        if (startMeasure)
            startMeasure = false;

        if (heartRateStatus != hs->mHeartRateStatus)
            hs->mStatusChanged = true;

        hs->mHeartRateStatus = heartRateStatus;
        hs->mTouchStatus = touchStatus;
        if (heartRate >= 0)
            hs->mHeartRate = heartRate;

        if (hs->mStatusChanged || heartRateChange) {
            char msg = 'h';
            retval = write(hs->mWorkerWriteFd, &msg, 1);
            if (retval < 0) {
                LOGE("Failed to write msg to pipe: %s", strerror(errno));
                continue;
            }
        }
    }

    return NULL;
}

int HeartRateSensorPixArt::read_gsensor_data()
{
    int ret = -1;

    sensor_data_t sensor;
    sensor_data_changed_t change;

    memset(&change, 0, sizeof(sensor_data_changed_t));

    ioctl(gsensor_fd, FRIZZ_IOCTL_GET_SENSOR_DATA_CHANGED, &change);
    if(change.updateflag1 & (1 << FRIZZ_OFFSET_ACCELEROMETER)) {
        memset(&sensor, 0x00, sizeof(sensor));
        sensor.code = SENSOR_TYPE_ACCELEROMETER;
        ret = ioctl(gsensor_fd, FRIZZ_IOCTL_SENSOR_GET_DATA, &sensor);
        if(!ret) {
#ifdef BOARD_IN901
            mGSensorData[0] = sensor.f32_value[0];
            mGSensorData[1] = sensor.f32_value[1];
            mGSensorData[2] = sensor.f32_value[2];
#else
            mGSensorData[0] = sensor.f32_value[0];
            mGSensorData[1] = sensor.f32_value[1];
            mGSensorData[2] = sensor.f32_value[2];
#endif

#ifdef DEBUG
            LOGI("====================================");
            LOGI("RawData X = %f", mGSensorData[0]);
            LOGI("RawData Y = %f", mGSensorData[1]);
            LOGI("RawData Z = %f", mGSensorData[2]);
            LOGI("====================================");
#endif

            transGSensorData();
#ifdef DEBUG
            LOGI("====================================");
            LOGI("Reg RawData X = 0x%04x", (int)mGSensorData[0] & 0xFFFF);
            LOGI("Reg RawData Y = 0x%04x", (int)mGSensorData[1] & 0xFFFF);
            LOGI("Reg RawData Z = 0x%04x", (int)mGSensorData[2] & 0xFFFF);
            LOGI("====================================");

            reverse_transGSensorData();
#endif
        } else {
            mGSensorData[0] = 0.6200982332229614;
            mGSensorData[1] = 1.655815124511719;
            mGSensorData[2] = 0.040701430290937424;
        }
    } else {
        ret = 1;
    }
    return ret;
}

int HeartRateSensorPixArt::open_gsensor_device(const char* dev_name)
{
    if (gsensor_fd<0 && dev_name) {
        gsensor_fd = open(dev_name, O_RDONLY);
        if(gsensor_fd < 0) {
            LOGE("Couldn't open %s (%s)", dev_name, strerror(errno));
            return -1;
        }
    }

    return 0;
}

int HeartRateSensorPixArt::readEvents(sensors_event_t* event, int count)
{
    if (!mEnabled || count <= 0) {
        return -EINVAL;
    }

    event->sensor = sSensor.handle;
    event->type = sSensor.type;
    event->data[0] = mHeartRate;

    if (mStatusChanged) {
        event->data[1] = mHeartRateStatus;
        mStatusChanged = false;
    } else {
        event->data[1] = 0;
    }

    return 1;
}

int HeartRateSensorPixArt::setDelay(int64_t ns)
{
    mDelay = NS_TO_MS(ns);
    LOGI("%s: set sample rate to : %lldms", mSensorName, mDelay);
    return 0;
}

int HeartRateSensorPixArt::enableDisable(int enable)
{
    int retval = 0;

    /*
     * Enable/Disable GSensor
     */

    if (enable) {
        if ((mSensorListener != NULL) && mSensorListener.get()) {
            retval = mSensorListener->enableSensor(SensorListener::SENSOR_ACCELEROMETER);
            if (retval != NO_ERROR) {
                mSensorListener.clear();
            }
        }
    } else {
        if (mSensorListener != NULL && mSensorListener.get()) {
            mSensorListener->disableSensor(SensorListener::SENSOR_ACCELEROMETER);
        }
    }

    LOGI("GSensor: set %sable", enable ? "en" : "dis");

    /*
     * Enable/Disable PixArt Sensor
     */
    retval = ioctl(dev_fd, PIXART_IOCTL_SENSOR_ENABLE_DISABLE, &enable);
    if (retval < 0) {
        LOGE("Failed to %sable sensor: %s", (enable == 1) ? "en" : "dis",
                strerror(errno));
        return retval;
    }
    return 0;
}

int HeartRateSensorPixArt::enable(int enabled)
{
    int retval = 0;

    if (enabled) {
        if (mEnabled) {
            LOGW("%s: already enable", mSensorName);
            return 0;
        }

        retval = enableDisable(enabled);
         if (retval < 0) {
             LOGE("Failed to enable %s: %s", PIXART_DEVICE_PATH, strerror(errno));
             return retval;
         }

        pthread_mutex_lock(&mLock);
        mStartThread = true;
        pthread_cond_signal(&mCond);
        pthread_mutex_unlock(&mLock);

        mEnabled = true;
        setDelay(PIXART_DEFAULT_DELAY);

    } else {
        if (!mEnabled) {
            LOGW("%s: already disable", mSensorName);
            return 0;
        }

        pthread_mutex_lock(&mLock);
        mStartThread = false;
        pthread_mutex_unlock(&mLock);

        retval = enableDisable(enabled);
        if (retval < 0) {
            LOGE("Failed to disable %s: %s", PIXART_DEVICE_PATH, strerror(errno));
            return retval;
        }
        mEnabled = false;
    }

    LOGI("%s: set %sable", mSensorName, mEnabled ? "en" : "dis");

    return 0;
}

bool HeartRateSensorPixArt::isExist() const
{
    return mExist;
}

int HeartRateSensorPixArt::populateSensorList(sensor_t* list) const
{
    memcpy(list, &sSensor, sizeof(struct sensor_t));
    return 1;
}

void HeartRateSensorPixArt::transGSensorData()
{
    for (int i = 0; i < AXIS_SIZE; i++) {
        do_transGSensorData(&mGSensorData[i]);
    }
}

void HeartRateSensorPixArt::do_transGSensorData(float *gsensorData)
{
    int sign = 0;
    int ex = 0;
    int count_ex = 0;
    float mant = 0;
    float changedData = 0;
    int *data = (int *)gsensorData;
    int tmp_data = 0;

    sign = get_sign(*data);
    ex = get_exponent(*data);
    mant = get_mantissa(*data);

    if (ex == 0 && mant == 0) {
        changedData = 0;
        *data = changedData;
        return;
    }

    count_ex = ex - EX_PARAM + (FULL_BIT - 1) + SENSITIVITY;
    changedData = mant * _2pow(count_ex);

    *data = (short)changedData & 0xFFFF;
    //GSensor data transform 16G to 4G
    *data = *data << 2;

    if (sign == 1) {
        two_complement(data);
        tmp_data = *data | 0xFFFF8000;
    } else {
        tmp_data = (*data & 0x7FFF);
    }
    *gsensorData = tmp_data;
}

int HeartRateSensorPixArt::get_sign(int data)
{
    return (data & (0x1 << 31) ? 1 : 0);
}

int HeartRateSensorPixArt::get_exponent(int data)
{
    return (data >> 23) & (0xFF);
}

float HeartRateSensorPixArt::get_mantissa(int data)
{
    int i = 0;
    int ex = 0;
    float retval = 0;

    for (i = 22; i >= 0; i--) {
        retval += (data & (0x1 << i) ? 1 : 0) * powf(2, i - 23);
    }

    ex = (data >> 23) & (0xFF);
    if (ex == 0xFF || ex == 0) {
//        LOGI("exponent full 1 or full 0\n");
    } else {
        retval += 1;
    }

//    LOGI("get_mantissa   %f\n", retval);
    return retval;
}

float HeartRateSensorPixArt::_2pow(int data)
{
    float retval = -1;

    switch(data) {
    case 0:
        retval = 1.0;
        break;
    case 1:
        retval = 2.0;
        break;
    case 2:
        retval = 4.0;
        break;
    case 3:
        retval = 8.0;
        break;
    case 4:
        retval = 16.0;
        break;
    case 5:
        retval = 32.0;
        break;
    case 6:
        retval = 64.0;
        break;
    case 7:
        retval = 128.0;
        break;
    case 8:
        retval = 256.0;
        break;
    case 9:
        retval = 512.0;
        break;
    case 10:
        retval = 1024.0;
        break;
    case -1:
        retval = 0.5;
        break;
    case -2:
        retval = 0.25;
        break;
    case -3:
        retval = 0.125;
        break;
    case -4:
        retval = 0.0625;
        break;
    case -5:
        retval = 0.03125;
        break;
    case -6:
        retval = 0.015625;
        break;
    case -7:
        retval = 0.0078125;
        break;
    case -8:
        retval = 0.00390625;
        break;
    case -9:
        retval = 0.001953125;
        break;
    case -10:
        retval = 0.000976562;
        break;
    default:
        retval = powf(2, data);
        break;
    }

    return retval;
}

void HeartRateSensorPixArt::two_complement(int *data)
{
    *data = (*data & 0xFFFF);
    *data = (~(*data & 0x7FFF) + 1) & 0x7FFF;
}

void HeartRateSensorPixArt::reverse_transGSensorData()
{
    int i = 0;
    float retval[3] = {0.0};

    for (i = 0; i < AXIS_SIZE; i++) {
        retval[i] = do_reverse_transGSensorData(mGSensorData[i]);
    }

    LOGI("====================================");
    LOGI("Reverse Data X = %f\n", retval[0]);
    LOGI("Reverse Data Y = %f\n", retval[1]);
    LOGI("Reverse Data Z = %f\n", retval[2]);
    LOGI("====================================");
}

float HeartRateSensorPixArt::do_reverse_transGSensorData(float gsensorData)
{
    int sign = 0;
    int ex = 0;
    int mant = 0;
    int count_ex = 0;
    int remain = 0;
    int result = 0;
    int tmp_data;

    tmp_data = (int)gsensorData;
    sign = (tmp_data & (0x1 << 15)) ? 1 : 0;
    if (sign == 1) {
        two_complement(&tmp_data);
        tmp_data = tmp_data & 0x7FFF;
    }
    tmp_data = tmp_data >> 2;
    tmp_data = tmp_data & 0xFFFF7FFF;

    while (tmp_data >= 2 && count_ex < 23) {
        remain = tmp_data % 2;
        tmp_data /= 2;
        mant |= (0x1 & remain) << count_ex;
        count_ex++;
    }

    ex = count_ex - (FULL_BIT - 1) - SENSITIVITY + EX_PARAM;
    result = (sign << 31 | ex << 23 | mant << (22 - count_ex + 1));

#if 0
    LOGI("sign = %d\n", sign);
    LOGI("mant = 0x%04x\n", mant);
    LOGI("count_ex = %d\n", count_ex);
    LOGI("ex = %d\n", ex);
    LOGI("result = 0x%08x\n", result);
#endif
    return *((float *)&result);
}

