#ifndef _HEART_RATE_H_
#define _HEART_RATE_H_
#ifdef __cplusplus
extern "C" {
#endif

struct heart_rate_config {
	unsigned int avg_point;
	long avg_middle;

	long avg_heart_middle;
	unsigned int avg_heart_point;

	unsigned int first_drop_point;

	unsigned int result_len;
	unsigned int result_low_len;

	float max_hz;
	float min_hz;
	unsigned int dst_len;
	unsigned int src_len;
	unsigned int cal_period_ms;
	unsigned int period;
};

extern void heart_rate_reset_config(struct heart_rate_config *config);

extern int heart_rate_get_rate(void);
extern void heart_rate_on_data_event(unsigned long heart_data);
extern int start_heart_rate_module(struct heart_rate_config *config);
extern void stop_heart_rate_module(void);

extern int heart_rate_dev_set_power(unsigned int power);
extern int heart_rate_dev_get_rate(void);
extern void heart_rate_dev_on_heart_rate_is_ready(int heart_rate);
extern int init_heart_rate_dev_module(void);
extern void release_heart_rate_dev_module(void);
extern int heart_rate_dev_set_handler(void (*handle_func)(void *handler, int heart_rate), void *handler);

#ifdef __cplusplus
}
#endif
#endif /* _HEART_RATE_H_ */
