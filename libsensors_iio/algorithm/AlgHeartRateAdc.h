#ifndef ALG_HEART_RATE_ADC_H
#define ALG_HEART_RATE_ADC_H

//#include "../sensors.h"
#include <hardware/sensors.h>
#include <utils/Vector.h>
#include <math.h>

#define SAVE_BACKUP 1

#define HWMON_SENSOR_HARDWARE_MODULE_ID       "hwmonsensor"
/* #define HEARTBEATS_FAST_LIMITATION_US          272727272   //180 times per minute */
#define HEARTBEATS_FAST_LIMITATION_US          333333333   //180 times per minute
#define HEARTBEATS_ONE_HUNDRED_US              600000000   //100 times per minute
#define HEARTBEATS_HALF_HUNDRED_US             1200000000   //50 times per minute
/* #define HEARTBEATS_SLOWEST_LIMITATION_US       1714285715  //35 times per minute */
#define HEARTBEATS_SLOWEST_LIMITATION_US       1333333333  //45 times per minute
#define HEARTBEATS_RECOVERY_US                 50000000000  //5 second
#define FAST_SAMPLE_INTERVAL_US                (20000000ll)
#define USE_HWMON_SENSOR                       0

#define CONSECUTIVE_HEART_BEATS                2
#define MAX_COUNT_BEATS                        15

#define MIN_PEAK_VALUE                         200
#define MIN_DOUBT_PEAK_VALUE                   700
#define MAX_VALLEY_VALUE                       0

using namespace android;

class AlgHeartRateAdc {
public:
    static AlgHeartRateAdc* getInstance();

    void handleEvent(sensors_event_t *event);
    int getHeartRate() const;

private:
    AlgHeartRateAdc();
    virtual ~AlgHeartRateAdc();

    void initParams(bool reset);
    int lowPassFilter(int data);
    int highPassFilter(int data);
    void detectHeartBeat(sensors_event_t *event);
    void dump(int64_t heartbeats, int64_t consHeartbeats, int value);
    void resetdata(void);

    int mHeartBeatRate;

    //for low-pass filter
    int y1;
    int y2;
    int x[26];
    int n;

    //for high-pass filter
    int y3;
    int x3[66];
    int m;

    int64_t mLastBeatTimeForHBInterval;
    int mFilteredData2; // new low-pass filtered and doubled data
    int mFilteredData3; // new low-pass and high-pass filtered and doubled data
    int mLastFilteredValue;
    bool mIsIncreasing;
    int mFirstIncreasingValue;
    int64_t mBeatTime;
    int64_t mLastPeakTimestamp;
    int mLastIncreasingValue;
    bool mNewPeak;
    int mConsecutiveHeartBeats;
    int64_t mLastBeatTimestamp;
    Vector<int64_t> mHBPtsList;
    Vector<int64_t> mDTList;
    int64_t mHeartBeatsIntervalNs;
    float mCurDeltaValue;
    int mInsertBeats;
    sensors_event_t *mCurEvent;
    int64_t mHeartBeats;
    float mFilteredData; // original low-pass filtered  and doubled data
    int64_t mDelayUs;
    int64_t mSampleIntervalUs;
    FILE* mHwmonFile; // save original data
    FILE* mFHwmonFile; // save original low-pass filtered  and doubled data
    FILE* mFDHwmonFile; // save new low-pass filtered and doubled data
    FILE* mFDHwmonFile2; // save new low-pass and high-pass filtered and doubled data

};

#endif //ALG_HEART_RATE_ADC_H
