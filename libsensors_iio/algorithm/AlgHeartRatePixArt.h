#ifndef ALG_HEART_RATE_PIXART_H
#define ALG_HEART_RATE_PIXART_H

#include <hardware/sensors.h>

typedef struct {
    uint8_t PPG_Data[13] ;
    int MEMS_Data[3] ;
} ppg_mems_data_t;

class AlgHeartRatePixArt {
public:
    static AlgHeartRatePixArt* getInstance();
    int handleData(ppg_mems_data_t *rawData, float *gSensorData);
    void close();
    int SetMemsScale(int scale);
    void EnableFastOutput(int en);
    void EnableAutoMode(int en);
    void EnableMotionMode(int en);
    void Algorithm_Reset() const;
    int getHeartRateGrade() const;
    int getTouchStatus() const;

private:
    AlgHeartRatePixArt();
    virtual ~AlgHeartRatePixArt();

    void pixart_alg_PXIALGMOTION_Open(const int HZ) const;
    float verify_library() const;
    int pixart_alg_PXIALGMOTION_Process(const char input_ppg_data[],
            const float input_mems_data[]);

    int mHeartRate;
    int mHeartRateGrade;
    int mAlgVersion;
    int mPreFrameCount;
    int mTouchStatus;
    int mReadyStatus;
    struct timeval mTouchTime;
    struct timeval mReadyTime;
};

#endif //ALG_HEART_RATE_PIXART_H
