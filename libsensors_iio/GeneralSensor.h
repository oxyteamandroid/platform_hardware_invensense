/*
 * Copyright (C) 2008 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef ANDROID_GENERAL_SENSOR_H
#define ANDROID_GENERAL_SENSOR_H

#include <stdint.h>
#include <errno.h>
#include <sys/cdefs.h>
#include <sys/types.h>

#include "sensors.h"
#include "SensorBase.h"
#include "InputEventReader.h"
#include "sensor_params.h"

#define GENERAL_SYSCLS_NAME				    "i2c-adapter"
#define GENERAL_DEFAULT_DELAY                                  (5000000000) //5s

#define HEART_RATE_DEFAULT_DELAY            (40000000)  //40ms
#define STEP_COUNTER_DEFAULT_DELAY          (300000000) //300ms
#define TEMP_DEFAULT_DELAY                  (1000000000)//1s
#define HUMI_DEFAULT_DELAY                  (1000000000)//1s
#define PRESSURE_DEFAULT_DELAY              (1000000000)//1s
#define PROXIMITY_DELAY                     (80000000)//80ms
#define UV_DEFAULT_DELAY                    (1000000000)//1s

#define NS_TO_MS(n) (n / (1000 * 1000))
#define GET_ARRAY_LEN(array,len) {len = (sizeof(array) / sizeof(array[0]));}

/*****************************************************************************/

struct input_event;

static const int generalEventCount = 2;
typedef struct general_events_desc {
	const char *enable;
	const char *delay;
} general_events_desc_t;

typedef struct general_sensor_desc {
	const char *sensor_name;
	const char *sys_dev_path;
	const general_events_desc_t sensor_events;
	const int32_t event_type;
	const int32_t event_code;
	const int32_t sensor_handle;
	const int32_t sensor_type;
} general_sensor_desc_t;


class GeneralSensor : public SensorBase {

public:

	GeneralSensor(const char* basePath, general_sensor_desc_t* sensor_desc);
	~GeneralSensor();

	int enable(int enabled);
	int setDelay(int64_t ns);
	const char* getName(void);
	general_sensor_desc_t* getSensorDesc(void);
	int readEvents(sensors_event_t *data, int count);
	void setBasePath(char *path);
	int setDevicePath(const char *bpath, const char *rpath);

	inline int32_t getEventType(void) {return eventType;}
	inline int32_t getEventCode(void) {return eventCode;}
	inline int32_t getSensorHandle(void) {return sensorHandle;}
	inline int32_t getSensorType(void) {return sensorType;}
protected:
	general_sensor_desc_t *mSensor;
	int mEnabled;
	int64_t mDelay;
	int64_t mTimestamp;
	InputEventCircularReader mGeneralInputReader;
	char input_sysfs_path[PATH_MAX];
	int input_sysfs_path_len;

	int32_t eventType;
	int32_t eventCode;
	int32_t sensorHandle;
	int32_t sensorType;
};

/*****************************************************************************/

#endif
