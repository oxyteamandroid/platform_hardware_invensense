# Can't have both manta and non-manta libsensors.
ifeq ($(strip $(BOARD_HAL_SENSORS_USE_THIRD_PARTY)), true)
ifeq ($(strip $(BOARD_HAL_SENSORS_THIRD_PARTY)), invensense)
ifeq ($(strip $(BOARD_HAL_SENSORS_INVENSENSE_IIO)), true)
# libsensors_iio expects IIO drivers for an MPU6050+AK8963 which are only available on manta.
include $(call all-named-subdir-makefiles,libsensors_iio libsensors_iio/algorithm)
else
# libsensors expects an non-IIO MPU3050.
include $(call all-named-subdir-makefiles,mlsdk libsensors)
endif
endif
endif
